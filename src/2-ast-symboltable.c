/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>

#include "config.h"

#include "2-ast.h"

extern void yyerror(const char *s);

char tmp_str_1[2048], tmp_str_2[2048];

struct Symbol_Table *create_symbol_table(struct Symbol_Table *parent) {
  static int current_sym_table_idx = 0; /* for debugging purposes */
  struct Symbol_Table *sym_table = ALLOC(struct Symbol_Table, 1);
  sym_table->idx = current_sym_table_idx++;
  sym_table->parameter_memory = 0;
  sym_table->local_variable_memory = 0;
  sym_table->list = mate__create_list();
  sym_table->hashtable = mate__create_hashtable(SYMBOL_TABLE__HASH_TABLE_SIZE,
                                                mate__standard_hash_function);
  sym_table->parent = parent;
  sym_table->children = mate__create_list();
  if (parent != NULL)
    mate__push_to_list(sym_table->parent->children, sym_table);
  return sym_table;
}

void add_to_symbol_table(struct Symbol_Table *symbol_table,
                         struct Symbol *sym) {
  struct Symbol *existing_sym =
      mate__get_hashtable_item(symbol_table->hashtable, sym->id);
  if (existing_sym != NULL) {
    sprintf(tmp_str_1, "redefinition of '%s', already defined in line %d.",
            sym->id, existing_sym->src_line);
    yyerror(tmp_str_1);
  }
  mate__push_to_list(symbol_table->list, sym);
  mate__insert_into_hashtable(symbol_table->hashtable, sym->id, sym);
}
