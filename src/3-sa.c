/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <mate/list.h>

#include "3-sa.h"

extern void yyerror(const char *s);
extern char tmp_str_1[2048], tmp_str_2[2048];

// TODO: check function call (number of parameters and types), return (matches
// function), if is boolean, for-loop has integer ranges,  ...

#define TCMP_FIRST(T1, T2, T)                                                  \
  if ((lhs->datatype == T1 && rhs->datatype == T2) ||                          \
      (lhs->datatype == T2 && rhs->datatype == T1)) {                          \
    sym->datatype = T;                                                         \
  }
#define TCMP(T1, T2, T)                                                        \
  else if ((lhs->datatype == T1 && rhs->datatype == T2) ||                     \
           (lhs->datatype == T2 && rhs->datatype == T1)) {                     \
    sym->datatype = T;                                                         \
  }

int get_memory_size(struct Symbol *sym) {
  switch (sym->datatype) {
  case TYPE_BOOL:
    return 1;
  case TYPE_INT:
  case TYPE_REAL:
    return 8;
  default:
    print_abstract_syntax_tree();
    printf("symbol='%s', datatype=%d\n", sym->id, sym->datatype);
    ASSERT(false); /* unimplemented */
  }
  return 0; /* dead code */
}

void typecheck_binary_operation(struct Symbol *sym) {
  ASSERT(mate__get_list_length(sym->ast_children) == 2);
  struct Symbol *lhs = mate__get_list_item(sym->ast_children, 0);
  struct Symbol *rhs = mate__get_list_item(sym->ast_children, 1);
  struct Symbol *var;
  bool compatible;
  switch (sym->type) {
  case SYM_ADD:
  case SYM_SUBTRACT:
  case SYM_MULTIPLY:
  case SYM_DIVIDE:
    TCMP_FIRST(TYPE_BOOL, TYPE_BOOL, TYPE_BOOL)
    TCMP(TYPE_INT, TYPE_INT, TYPE_INT)
    TCMP(TYPE_REAL, TYPE_REAL, TYPE_REAL)
    TCMP(TYPE_REAL, TYPE_INT, TYPE_REAL)
    TCMP(TYPE_REAL, TYPE_BOOL, TYPE_REAL)
    TCMP(TYPE_INT, TYPE_BOOL, TYPE_INT)
    else {
      sprintf(tmp_str_1, "cannot perform '%s' on incompatible types %s",
              sym->id, get_datatype_name(lhs->datatype));
      sprintf(tmp_str_2, "%s and %s\n", tmp_str_1,
              get_datatype_name(rhs->datatype));
      yyerror(tmp_str_2);
    }
    sym->memory_size = 8; // TODO
    break;
  case SYM_GREATER:
  case SYM_GREATER_EQUAL:
  case SYM_LESS:
  case SYM_LESS_EQUAL:
  case SYM_EQUAL:
    TCMP_FIRST(TYPE_BOOL, TYPE_BOOL, TYPE_BOOL)
    TCMP(TYPE_INT, TYPE_INT, TYPE_BOOL)
    TCMP(TYPE_REAL, TYPE_REAL, TYPE_BOOL)
    TCMP(TYPE_REAL, TYPE_INT, TYPE_BOOL)
    TCMP(TYPE_REAL, TYPE_BOOL, TYPE_BOOL)
    TCMP(TYPE_INT, TYPE_BOOL, TYPE_BOOL)
    else {
      sprintf(tmp_str_1, "cannot perform '%s' on incompatible types %s",
              sym->id, get_datatype_name(lhs->datatype));
      sprintf(tmp_str_2, "%s and %s\n", tmp_str_1,
              get_datatype_name(rhs->datatype));
      yyerror(tmp_str_2);
    }
    break;
  case SYM_ASSIGNMENT:
    if (lhs->type != SYM_LOCAL_VARIABLE)
      yyerror("left hand side of the assignment must be a variable");
    var = get_symbol(sym->sym_table, lhs->id);
    if (var == NULL) {
      /* declare local variable */
      lhs->datatype = rhs->datatype;
      lhs->memory_size = get_memory_size(rhs);
      // mate__insert_into_hashtable(sym->sym_table->symbols, lhs->id, lhs); //
      // TODO: remove
      add_to_symbol_table(sym->sym_table, lhs);
    } else if (var->type != SYM_LOCAL_VARIABLE &&
               var->type != SYM_FUNCTION_PARAM) {
      sprintf(tmp_str_1, "'%s' is not a variable", var->id);
      yyerror(tmp_str_1);
    }
    sym->memory_size = 8; // TODO
    break;
  default:
    printf("typecheck_binary_operation: unimplemented type '%s'\n",
           get_symbol_type_name(sym->type));
    ASSERT(false); /* unimplemented */
  }
}
