/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>

#include <mate/common.h>

#include "config.h"

#include "6-mcg-iomcpu.h"

extern FILE *objfile;

int current_address = 0;

void iomcpu_encode_instruction(struct IOMCPU_Instruction *instr) {
  uint64_t code;
  switch (instr->opcode >> 4) {
  case 0: /* instruction type A */
    code = ((uint64_t)instr->opcode << (64 - 6)) |
           ((uint64_t)instr->op[0] & 0x03FFFFFFFFFFFFFF);
    break;
  case 1: /* instruction type B */
    code = ((uint64_t)instr->opcode << (64 - 6)) |
           ((uint64_t)instr->op[0] << (64 - 6 - 5)) |
           ((uint64_t)instr->op[1] & 0x001FFFFFFFFFFFFF);
    break;
  case 2: /* instruction type C */
    code = ((uint64_t)instr->opcode << (64 - 6)) |
           ((uint64_t)instr->op[0] << (64 - 6 - 5)) |
           ((uint64_t)instr->op[1] << (64 - 6 - 5 - 5)) |
           ((uint64_t)instr->op[2] & 0x0000FFFFFFFFFFFF);
    break;
  case 3: /* instruction type D */
    code = ((uint64_t)instr->opcode << (64 - 6)) |
           ((uint64_t)instr->op[0] << (64 - 6 - 5)) |
           ((uint64_t)instr->op[1] << (64 - 6 - 5 - 5)) |
           ((uint64_t)instr->op[2] << (64 - 6 - 5 - 5 - 5)) |
           ((uint64_t) /*val43*/ 0x0 & 0x000007FFFFFFFFFF);
    break;
  default:
    assert(false);
  }
  instr->encoded = code;
}

void iomcpu_decode_instruction(struct IOMCPU_Instruction *dest, uint64_t code) {
  dest->opcode = (code >> (64 - 6)) & (uint64_t)0x3F;
  switch (dest->opcode >> 4) {
  case 0: /* instruction type A */
    dest->op[0] = code & (uint64_t)0x03FFFFFFFFFFFFFF;
    break;
  case 1: /* instruction type B */
    dest->op[0] = (code >> (64 - 6 - 5) & (uint64_t)0x1F);
    dest->op[1] = code & (uint64_t)0x001FFFFFFFFFFFFF;
    break;
  case 2: /* instruction type C */
    dest->op[0] = (code >> (64 - 6 - 5) & (uint64_t)0x1F);
    dest->op[1] = (code >> (64 - 6 - 5 - 5) & (uint64_t)0x1F);
    dest->op[2] = code & (uint64_t)0x0000FFFFFFFFFFFF;
    break;
  case 3: /* instruction type D */
    dest->op[0] = (code >> (64 - 6 - 5) & (uint64_t)0x1F);
    dest->op[1] = (code >> (64 - 6 - 5 - 5) & (uint64_t)0x1F);
    dest->op[2] = (code >> (64 - 6 - 5 - 5 - 5) & (uint64_t)0x1F);
    break;
  default:
    assert(false);
  }
}

struct IOMCPU_Instruction *push_iomcpu_instruction(struct Basicblock *bb,
                                                   enum IOMCPU_Opcode opcode,
                                                   int64_t op1, int64_t op2,
                                                   int64_t op3) {
  struct IOMCPU_Instruction *instr = ALLOC(struct IOMCPU_Instruction, 1);
  mate__push_to_list(bb->machine_code, instr);
  instr->address = current_address++;
  instr->opcode = opcode;
  instr->op[0] = op1;
  instr->op[1] = op2;
  instr->op[2] = op3;
  instr->dest_basicblock = NULL;
  iomcpu_encode_instruction(instr);
  return instr;
}

#define MAX_REG 64                   // TODO: too small??
int ic2mc_register_mapping[MAX_REG]; // TODO: comments

void generate_basicblock_iom_cpu(struct Symbol *sym, struct Basicblock *bb) {
  /*TODO
  int current_reg = 0; / * current register * /

  for (int i = 0; i < MAX_REG; i++)
    ic2mc_register_mapping[i] = i;

  struct IC_Instruction *ic_instr;
  struct IOMCPU_Instruction *mc_instr;
  struct mate__List_iterator ic_iterator =
      mate__create_list_iterator(bb->intermediate_code);
  while ((ic_instr = mate__get_next_list_iterator_item(&ic_iterator)) != NULL) {
    switch (ic_instr->opc) {
    case INSTR_ADD:
      mc_instr = push_iomcpu_instruction(
          bb, IOMCPU_OPC_ADD, ic2mc_register_mapping[ic_instr->op[0]],
          ic2mc_register_mapping[ic_instr->op[1]],
          ic2mc_register_mapping[ic_instr->op[2]]);
      // TODO ic_instr->align > 8
      break;
    case INSTR_SUB:
      mc_instr = push_iomcpu_instruction(
          bb, IOMCPU_OPC_SUB, ic2mc_register_mapping[ic_instr->op[0]],
          ic2mc_register_mapping[ic_instr->op[1]],
          ic2mc_register_mapping[ic_instr->op[2]]);
      // TODO ic_instr->align > 8
      break;
    case INSTR_MUL:
      mc_instr = push_iomcpu_instruction(
          bb, IOMCPU_OPC_MUL, ic2mc_register_mapping[ic_instr->op[0]],
          ic2mc_register_mapping[ic_instr->op[1]],
          ic2mc_register_mapping[ic_instr->op[2]]);
      // TODO ic_instr->align > 8
      break;
    case INSTR_DIV:
      mc_instr = push_iomcpu_instruction(
          bb, IOMCPU_OPC_DIV, ic2mc_register_mapping[ic_instr->op[0]],
          ic2mc_register_mapping[ic_instr->op[1]],
          ic2mc_register_mapping[ic_instr->op[2]]);
      // TODO ic_instr->align > 8
      break;
    case INSTR_CMP_GREATER:

      op[0]
          : = IOMCPU_OPC_IS_POSITIVE(IOMCPU_OPC_CMP(op[1], op[2] + 1))

                mc_instr = push_iomcpu_instruction(
                    bb, IOMCPU_OPC_CMP, ic2mc_register_mapping[ic_instr->op[0]],
                    ic2mc_register_mapping[ic_instr->op[1]],
                    ic2mc_register_mapping[ic_instr->op[2]]);
      // TODO ic_instr->align > 8
      break;
    case INSTR_CMP_LESS:

      op[0]
          : = IOMCPU_OPC_IS_POSITIVE(IOMCPU_OPC_CMP(op[2], op[1] + 1))

                mc_instr = push_iomcpu_instruction(
                    bb, IOMCPU_OPC_CMP, ic2mc_register_mapping[ic_instr->op[1]],
                    ic2mc_register_mapping[ic_instr->op[0]],
                    ic2mc_register_mapping[ic_instr->op[2]]);
      // TODO ic_instr->align > 8
      break;
    case INSTR_CMP_GREATER_EQUAL:

      op[0] : = IOMCPU_OPC_IS_POSITIVE(IOMCPU_OPC_CMP(op[1], op[2]))

                  break;
    case INSTR_CMP_LESS_EQUAL:

      op[0] : = IOMCPU_OPC_IS_POSITIVE(IOMCPU_OPC_CMP(op[2], op[1]))

                  assert(false); // TODO
      break;
    case INSTR_LOAD:
      assert(false); // TODO
      break;
    case INSTR_STORE:
      assert(false); // TODO
      break;
    case INSTR_LOAD_IMMEDIATE:
      mc_instr = push_iomcpu_instruction(
          bb, IOMCPU_OPC_LOAD_IMMEDIATE,
          ic2mc_register_mapping[ic_instr->op[0]], ic_instr->op[1],
          0); // TODO: IOMCPU_OPC_LOAD_UPPER_I, if needed
      break;
    case INSTR_LOAD_LOCAL:
      break;
    case INSTR_STORE_LOCAL:
      break;
    case INSTR_BRANCH:
      mc_instr =
          push_iomcpu_instruction(bb, IOMCPU_OPC_JUMP, ic_instr->op[0], 0, 0);
      break;
    case INSTR_BRANCH_EQUAL:
      assert(false); // TODO
      break;
    case INSTR_BRANCH_NOT_EQUAL:
      assert(false); // TODO
      break;
    case INSTR_BRANCH_IF_ONE:
      assert(false); // TODO
      break;
    case INSTR_BRANCH_IF_ZERO:
      assert(false); // TODO
      break;
    case INSTR_CALL:
      mc_instr = push_iomcpu_instruction(bb, IOMCPU_OPC_CALL, 0, 0, 0);
      mc_instr->dest_basicblock = ic_instr->basicblock;
      break;
    case INSTR_RETURN:
      mc_instr = push_iomcpu_instruction(bb, IOMCPU_OPC_RETURN, 0, 0, 0);
      break;
    case INSTR_RETURN_WITH_VALUE:
      assert(false); // TODO
      break;
    case INSTR_NOP:
      / * skip * /
      break;
    case INSTR_ALLOC_STACK:
      mc_instr = push_iomcpu_instruction(bb, IOMCPU_OPC_LOAD_IMMEDIATE,
                                         current_reg, ic_instr->op[0], 0);
      mc_instr =
          push_iomcpu_instruction(bb, IOMCPU_OPC_SUB, IOMCPU_REG__STACK_POINTER,
                                  IOMCPU_REG__STACK_POINTER, current_reg);
      break;
    case INSTR_PUSH:
      assert(false); // TODO
      break;
    case INSTR_POP:
      mc_instr = push_iomcpu_instruction(bb, IOMCPU_OPC_LOAD_IMMEDIATE,
                                         current_reg, ic_instr->op[0], 0);

      mc_instr =
          push_iomcpu_instruction(bb, IOMCPU_OPC_ADD, IOMCPU_REG__STACK_POINTER,
                                  IOMCPU_REG__STACK_POINTER, current_reg);
      break;
    case INSTR_MOVE:
      assert(false); // TODO
      break;
    default:
      assert(false); / * unimplemented * /
    }
}*/
}

void generate_machine_code__iom_cpu(struct Symbol *sym) {
  /* for each basicnblock: generate machine code */
  struct mate__List_iterator basicblock_iterator =
      mate__create_list_iterator(sym->code_basicblocks);
  struct Basicblock *bb;
  while ((bb = mate__get_next_list_iterator_item(&basicblock_iterator)) !=
         NULL) {
    generate_basicblock_iom_cpu(sym, bb);
  }
}

void write_machine_code__iom_cpu(struct Symbol *ast_root) {
  assert(ast_root != NULL);
  /* ----- for all functions ----- */
  struct mate__List_iterator ast_iterator =
      mate__create_list_iterator(ast_root->ast_children);
  struct Symbol *sym;
  while ((sym = mate__get_next_list_iterator_item(&ast_iterator)) != NULL) {
    if (sym->type != SYM_FUNCTION)
      continue;
    /* ----- for all basic blocks ----- */
    struct mate__List_iterator basicblock_iterator =
        mate__create_list_iterator(sym->code_basicblocks);
    struct Basicblock *bb;
    while ((bb = mate__get_next_list_iterator_item(&basicblock_iterator)) !=
           NULL) {
      /* ----- for all machine code instructions ----- */
      struct mate__List_iterator mc_iterator =
          mate__create_list_iterator(bb->machine_code);
      struct IOMCPU_Instruction *mc_instr;
      while ((mc_instr = mate__get_next_list_iterator_item(&mc_iterator)) !=
             NULL) {
        /* ----- fix jump target references ----- */
        struct IOMCPU_Instruction *mc_dest_instr;
        switch (mc_instr->opcode) {
        case IOMCPU_OPC_CALL:
          mc_dest_instr = mate__get_first_list_item(
              mc_instr->dest_basicblock->machine_code);
          assert(mc_dest_instr != NULL);
          mc_instr->op[1] = mc_dest_instr->address;
          break;
        default:;
        }
        /* ----- write output ----- */
        fwrite(&mc_instr->encoded, sizeof(uint64_t), 1, objfile);
      }
    }
  }
}
