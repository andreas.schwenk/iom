/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>

#include <mate/list.h>

#include "5-opt.h"

#include "4-icg.h"

extern struct Symbol *ast; /* abstract syntax tree */

int current_reg; // TODO
int current_addr = 0;
/* current intermediate code address */ // TODO: reset in main (create init
                                        // function)!!

struct IC_Instruction *push_ic_instruction(struct mate__List *list,
                                           struct Symbol *sym,
                                           enum IC_Instruction_Type opc,
                                           uint64_t op0, uint64_t op1,
                                           uint64_t op2) {
  /* create new instructio */
  struct IC_Instruction *instr = ALLOC(struct IC_Instruction, 1);
  instr->sym = sym;
  instr->addr = 0;
  instr->opc = opc;
  instr->op[0] = op0;
  instr->op[1] = op1;
  instr->op[2] = op2;
  instr->align = sym->memory_size;
  instr->is_jump_dest = false;
  instr->dest_instr = NULL;
  /* push to instruction list */
  struct IC_Instruction *last_instr = mate__get_last_list_item(list);
  if (last_instr != NULL && last_instr->opc == INSTR_NOP)
    memcpy(last_instr, instr, sizeof(struct IC_Instruction));
  else
    mate__push_to_list(list, instr);
  /* return instruction */
  return instr;
}

void link_instructions(struct IC_Instruction *src,
                       struct IC_Instruction *dest) {
  src->dest_instr = dest;
  dest->is_jump_dest = true;
}

struct IC_Instruction *get_first_instruction_of_function(char *function_id) {
  struct Symbol *sym_fct =
      mate__get_hashtable_item(ast->sym_table->hashtable, function_id);
  assert(sym_fct->type == SYM_FUNCTION);
  assert(sym_fct != NULL);
  assert(mate__get_list_length(sym_fct->intermediate_code) > 0);
  return (struct IC_Instruction *)sym_fct->intermediate_code->first->data;
}

void generate_intermediate_code_rec(struct Symbol *root, struct Symbol *sym) {
  struct IC_Instruction *instr, *instr2 = NULL;
  struct IC_Instruction *for_loop_step_2_instr, *for_loop_step_5_instr,
      *for_loop_branch_instr;
  struct mate__List_iterator iterator;
  struct Symbol *child, *lhs;
  struct Symbol *for_loop_var, *for_loop_lower_bound, *for_loop_upper_bound;
  int i;
  int push_memory_size;
  bool generate;

  /* run recursively, depth-first */
  switch (sym->type) {
  case SYM_FUNCTION:
    push_ic_instruction(root->intermediate_code, sym, INSTR_ALLOC_STACK,
                        sym->sym_table->local_variable_memory, 0, 0);
    iterator = mate__create_list_iterator(sym->ast_children);
    generate = false;
    /* run recursively for all non-parameter children */
    while ((child = mate__get_next_list_iterator_item(&iterator)) != NULL) {
      if (generate)
        generate_intermediate_code_rec(root, child);
      else if (child->type == SYM_BEGIN)
        generate = true;
    }
    break;
  case SYM_FUNCTION_CALL:
    // TODO later: push variables that do not fit into one register
    iterator = mate__create_list_iterator(sym->ast_children);
    push_memory_size = 0;
    while ((child = mate__get_next_list_iterator_item(&iterator)) != NULL) {
      generate_intermediate_code_rec(root, child);
      push_ic_instruction(root->intermediate_code, child, INSTR_PUSH,
                          current_reg - 1, 0, 0);
      push_memory_size++; // TODO: depends on type
      current_reg--;
    }
    link_instructions(
        push_ic_instruction(root->intermediate_code, sym, INSTR_CALL, 0, 0, 0),
        get_first_instruction_of_function(sym->id));
    push_ic_instruction(root->intermediate_code, sym, INSTR_POP,
                        push_memory_size, 0, 0);
    current_reg++;
    break;
  case SYM_ASSIGNMENT:
    /* only generate code for right-hand side */
    ASSERT(mate__get_list_length(sym->ast_children) >= 2);
    generate_intermediate_code_rec(root,
                                   mate__get_list_item(sym->ast_children, 1));
    break;
  case SYM_IF_STATEMENT:
    // TODO: documentation (compare to SYM_FOR_LOOP below)
    iterator = mate__create_list_iterator(sym->ast_children);
    i = 0;
    instr = NULL;
    while ((child = mate__get_next_list_iterator_item(&iterator)) != NULL) {
      generate_intermediate_code_rec(root, child);
      if (i == 0)
        instr = push_ic_instruction(root->intermediate_code, sym,
                                    INSTR_BRANCH_IF_ZERO, current_reg, 0,
                                    /* address must be fixed later */ 0);
      i++;
    }
    link_instructions(instr, push_ic_instruction(root->intermediate_code, sym,
                                                 INSTR_NOP, 0, 0, 0));
    break;
  case SYM_FOR_LOOP:
    ASSERT(mate__get_list_length(sym->ast_children) >= 3);
    for_loop_var = mate__get_list_item(sym->ast_children, 0);
    for_loop_lower_bound = mate__get_list_item(sym->ast_children, 1);
    for_loop_upper_bound = mate__get_list_item(sym->ast_children, 2);
    /* (1.) initialize loop variable with lower bound */
    generate_intermediate_code_rec(root, for_loop_lower_bound);
    push_ic_instruction(root->intermediate_code, for_loop_var,
                        INSTR_STORE_LOCAL, 0, for_loop_var->memory_offset, 0);
    current_reg = 0;
    /* (2.) exit loop, i.e. jump to (6.) if value of loop variable is greater
     * than the upper bound */
    for_loop_step_2_instr = push_ic_instruction(
        root->intermediate_code, for_loop_var, INSTR_LOAD_LOCAL, 0,
        for_loop_var->memory_offset, 0);
    current_reg++;
    generate_intermediate_code_rec(root, for_loop_upper_bound);
    push_ic_instruction(root->intermediate_code, for_loop_var,
                        INSTR_CMP_GREATER_EQUAL, 1, 0, 1);
    for_loop_branch_instr = push_ic_instruction(
        root->intermediate_code, for_loop_var, INSTR_BRANCH_IF_ZERO, 0, 0,
        /* address must be fixed later */ 0);
    /* (3.) generate loop body */
    iterator = mate__create_list_iterator(sym->ast_children);
    i = 0;
    while ((child = mate__get_next_list_iterator_item(&iterator)) != NULL) {
      if (i > 2)
        generate_intermediate_code_rec(root, child);
      i++;
    }
    /* (4.) increment loop variable */
    push_ic_instruction(root->intermediate_code, for_loop_var, INSTR_LOAD_LOCAL,
                        0, for_loop_var->memory_offset, 0);
    push_ic_instruction(root->intermediate_code, for_loop_var,
                        INSTR_LOAD_IMMEDIATE, 1, 0, 0);
    push_ic_instruction(root->intermediate_code, for_loop_var, INSTR_ADD, 0, 0,
                        1);
    push_ic_instruction(root->intermediate_code, for_loop_var,
                        INSTR_STORE_LOCAL, 0, for_loop_var->memory_offset, 0);
    /* (5.) jump to the condition in (2.) */
    link_instructions(push_ic_instruction(root->intermediate_code, for_loop_var,
                                          INSTR_BRANCH, 0, 0, 0),
                      for_loop_step_2_instr);
    /* (6.) generate a no-operation instruction as temporary jump target */
    link_instructions(
        for_loop_branch_instr,
        push_ic_instruction(root->intermediate_code, sym, INSTR_NOP, 0, 0, 0));
    break;
  default:
    /* run recursively */
    iterator = mate__create_list_iterator(sym->ast_children);
    while ((child = mate__get_next_list_iterator_item(&iterator)) != NULL)
      generate_intermediate_code_rec(root, child);
    break;
  }

  // TODO: TYPES!!!!!

  switch (sym->type) {
  case SYM_CONST_INTEGER:
    push_ic_instruction(root->intermediate_code, sym, INSTR_LOAD_IMMEDIATE,
                        current_reg, sym->value_int, /*empty*/ 0);
    current_reg++;
    break;
  case SYM_CONST_REAL:
    ASSERT(false); // TODO
    break;
  case SYM_LOCAL_VARIABLE:
  case SYM_FUNCTION_PARAM:
    push_ic_instruction(root->intermediate_code, sym, INSTR_LOAD_LOCAL,
                        current_reg, sym->memory_offset, 0);
    current_reg++;
    break;
  case SYM_ADD:
    push_ic_instruction(root->intermediate_code, sym, INSTR_ADD,
                        current_reg - 2, current_reg - 2, current_reg - 1);
    current_reg--;
    break;
  case SYM_SUBTRACT:
    push_ic_instruction(root->intermediate_code, sym, INSTR_SUB,
                        current_reg - 2, current_reg - 2, current_reg - 1);
    current_reg--;
    break;
  case SYM_MULTIPLY:
    push_ic_instruction(root->intermediate_code, sym, INSTR_MUL,
                        current_reg - 2, current_reg - 2, current_reg - 1);
    current_reg--;
    break;
  case SYM_DIVIDE:
    push_ic_instruction(root->intermediate_code, sym, INSTR_DIV,
                        current_reg - 2, current_reg - 2, current_reg - 1);
    current_reg--;
    break;
  case SYM_GREATER:
    push_ic_instruction(root->intermediate_code, sym, INSTR_CMP_GREATER,
                        current_reg - 2, current_reg - 2, current_reg - 1);
    current_reg--;
    break;
  case SYM_ASSIGNMENT:
    lhs = sym->ast_children->first->data;
    push_ic_instruction(root->intermediate_code, lhs, INSTR_STORE_LOCAL,
                        current_reg - 1, lhs->memory_offset, 0);
    break;
  case SYM_RETURN:
    push_ic_instruction(
        root->intermediate_code, sym,
        current_reg > 0 ? INSTR_RETURN_WITH_VALUE : INSTR_RETURN, 0, 0, 0);
    break;
  case SYM_IF_STATEMENT:
  case SYM_FOR_LOOP:
  case SYM_FUNCTION:
  case SYM_FUNCTION_CALL:
  case SYM_BEGIN:
    /* do nothing */
    break;
  default:
    printf("---- icg for '%s' is unimplemented ----\n",
           get_symbol_type_name(sym->type)); // TODO: remove this
    ASSERT(false);                           /* unimplemented */
  }
  /* reset register stack */
  if (sym->ast_parent != NULL && (sym->ast_parent->type == SYM_FUNCTION ||
                                  sym->ast_parent->type == SYM_IF_STATEMENT ||
                                  sym->ast_parent->type == SYM_FOR_LOOP)) {
    current_reg = 0;
  }
}

void generate_symbol_addresses(struct Symbol_Table *sym_table,
                               int *parameter_address, int *variable_address) {
  struct mate__List_iterator iterator =
      mate__create_list_iterator(sym_table->list);
  struct Symbol *sym;
  while ((sym = mate__get_next_list_iterator_item(&iterator)) != NULL) {
    switch (sym->type) {
    case SYM_LOCAL_VARIABLE:
      *variable_address -= sym->memory_size;
      sym->memory_offset = *variable_address;
      break;
    case SYM_FUNCTION_PARAM:
      sym->memory_offset = *parameter_address;
      *parameter_address += sym->memory_size;
      break;
    default:
      ASSERT(false); /* unimplemented */
    }
  }
  /* run recursively for all child symbol tables */
  struct mate__List_iterator list_iterator =
      mate__create_list_iterator(sym_table->children);
  struct Symbol_Table *child;
  while ((child = mate__get_next_list_iterator_item(&list_iterator)) != NULL) {
    generate_symbol_addresses(child, parameter_address, variable_address);
  }
}

void generate_intermediate_code(struct Symbol *sym) {
  current_reg = 0;
  /* generate addresses for local variables */
  int parameter_address = 0; /* positive */
  int variable_address = 0;  /* negative */
  generate_symbol_addresses(sym->sym_table, &parameter_address,
                            &variable_address);
  sym->sym_table->parameter_memory = parameter_address;
  sym->sym_table->local_variable_memory = -variable_address;
  /* generate code */
  generate_intermediate_code_rec(sym, sym);
  /* [for debugging only:] generate addresses for instructions */
  struct mate__List_iterator iterator;
  struct IC_Instruction *instr;
  iterator = mate__create_list_iterator(sym->intermediate_code);
  while ((instr = mate__get_next_list_iterator_item(&iterator)) != NULL)
    instr->addr = current_addr++;
  /* [for debugging only:]  fix branch addresses */
  iterator = mate__create_list_iterator(sym->intermediate_code);
  while ((instr = mate__get_next_list_iterator_item(&iterator)) != NULL)
    if (instr->dest_instr != NULL)
      instr->op[2] = instr->dest_instr->addr;
  /* optimize intermediate code */
  optimize_intermediate_code(sym);
}
