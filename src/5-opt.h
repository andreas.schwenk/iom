/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== OPT := INTERMEDIATE CODE OPTIMIZATION ============================== */

#ifndef __OPT__H
#define __OPT__H

#include <inttypes.h>

#include <mate/common.h>

#include "4-icg.h"

/* ----- intermediate code optimization ------------------------------------- */

// TODO
struct Basicblock {
  struct mate__List *intermediate_code;
  struct mate__List *machine_code;
};

// TODO
void optimize_intermediate_code(struct Symbol *sym);

/* -------------------------------------------------------------------------- */

#endif
