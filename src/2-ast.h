/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== PARSER ACTION CODE / ABSTRACT SYNTAX TREE ========================== */

#ifndef __AST__H
#define __AST__H

#include <inttypes.h>

#include <mate/common.h>
#include <mate/hash.h>

#define top_of_block_stack()                                                   \
  ((struct Symbol *)mate__get_last_list_item(block_stack))

/* ----- symbol definitions ------------------------------------------------- */

// TODO
enum Symbol_Type {
  SYM_ROOT = 0,
  SYM_FUNCTION = 1,
  SYM_FUNCTION_PARAM = 2,
  SYM_BEGIN = 3, // TODO: also add to loops, if-statements, ...
  SYM_CONST_INTEGER = 4,
  SYM_CONST_REAL = 5,
  SYM_LOCAL_VARIABLE = 6,

  SYM_ASSIGNMENT = 7,
  SYM_ADD_ASSIGNMENT = 8,
  SYM_SUB_ASSIGNMENT = 9,
  SYM_MUL_ASSIGNMENT = 10,
  SYM_DIV_ASSIGNMENT = 11,

  SYM_EQUAL = 12,
  SYM_LESS = 13,
  SYM_GREATER = 14,
  SYM_LESS_EQUAL = 15,
  SYM_GREATER_EQUAL = 16,

  SYM_ADD = 17,
  SYM_SUBTRACT = 18,
  SYM_MULTIPLY = 19,
  SYM_DIVIDE = 20,

  SYM_FOR_LOOP = 21,
  SYM_FUNCTION_CALL = 22,
  SYM_RETURN = 23,
  SYM_MODULE = 24,
  SYM_IF_STATEMENT = 25,

  SYM_BLOCK = 26
};

// TODO
enum Type { TYPE_UNDEFINED = 0, TYPE_BOOL = 1, TYPE_INT = 2, TYPE_REAL = 3 };

struct Symbol;
struct Symbol_Table;

struct Symbol {
  char *id;
  int type; /* enum Symbol_Type */

  int datatype;

  int value_int;
  double value_real;

  int memory_offset; /* code generation */
  int memory_size;   /* code generation */

  int src_line; /* line number in source file */

  struct Symbol *ast_parent;
  struct mate__List *ast_children; /* used in Abstract Syntax Tree (AST) */

  struct Symbol_Table *sym_table;

  struct mate__List
      *intermediate_code; /* list of intermediate code instructions */
  struct mate__List *code_basicblocks; /* list of code basicblocks (intermediate
                                          and machine code) */
};

/**
 * create_symbol - symbol constructor
 * @id:            identifier
 * @type:          type
 * @ast_parent:    parent symbol in AST
 * @sym_table:     symbol table
 */
struct Symbol *create_symbol(char *id, int type, struct Symbol *ast_parent,
                             struct Symbol_Table *sym_table);

// TODO
char *get_datatype_name(enum Type type);

// TODO
char *get_symbol_type_name(enum Symbol_Type type);

/* ----- symbol table ------------------------------------------------------- */

struct Symbol_Table {
  struct mate__List
      *list; /* linked list in order of appearance in input source file */
  struct mate__Hashtable
      *hashtable;       /* same content as linked list; fast access */
  int parameter_memory; /* memory for parameters in bytes, including children */
  int local_variable_memory; /* memory for local variables in bytes, including
                                children */
  struct Symbol_Table *parent;
  struct mate__List *children; /* struct Symbol_Table */
  int idx;
};

// TODO
struct Symbol_Table *create_symbol_table(struct Symbol_Table *parent);

// TODO
void add_to_symbol_table(struct Symbol_Table *symbol_table, struct Symbol *sym);

// TODO
struct Symbol *get_symbol(struct Symbol_Table *leaf, char *var_id);

// TODO
void print_symbol_table(struct Symbol_Table *symbol_table, int indent);

/* ----- abstract syntax tree ----------------------------------------------- */

// TODO
void print_abstract_syntax_tree(void);

/**
 * init_parser - initializes parser
 */
void init_parser(void);

/**
 * release_parser - destroys parser
 */
void release_parser(void);

// TODO
void push_type(enum Type type);

/**
 * begin_function - adds a new function to AST
 * @id:            identifier of the new function
 */
void begin_function(char *id);

// TODO
void end_of_function_parameter_declaration(void);

// TODO
void end_function(void);

// TODO
void begin_for_loop(char *loop_var);

// TODO
void end_for_loop(void);

// TODO
void begin_if_statement(void);

// TODO
void end_if_statement(void);

// TODO
void push_function_return_type(void);

/**
 * push_function_parameter - adds a new parameter to the current parsed function
 * @id:            identifier of the new parameter
 */
void push_function_parameter(char *id);

/*// TODO
void push_block(void);

// TODO
void end_block(void);*/

// TODO
void push_const_int(int value);

// TODO
void push_const_real(double value);

// TODO
void push_variable(char *id);

// TODO
void push_function_call(char *id);

// TODO
void push_function_argument(void);

// TODO
void push_binary_operation(char *op);

// TODO
void push_expression(void);

// TODO
void push_module(char *id);

// TODO
void push_return(bool has_value);

/* -------------------------------------------------------------------------- */

#endif
