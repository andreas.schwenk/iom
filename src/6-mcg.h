/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== MCG := MACHINE CODE GENERATION ===================================== */

#ifndef __MCG__H
#define __MCG__H

#include <inttypes.h>

#include <mate/common.h>

#include "5-opt.h"

/* ----- machine code generation -------------------------------------------- */

// TODO
enum BACKEND_ARCHITECTURE {
  BACKEND_ARCHITECTURE_IOM_CPU = 0,
  BACKEND_ARCHITECTURE_LLVM_IC = 1
};

// TODO
void generate_machine_code(struct Symbol *sym);

// TODO
void print_machine_code(struct Symbol *sym, int indent);

// TODO
void write_machine_code(void);

// TODO
// void print_mc_instruction();

/* -------------------------------------------------------------------------- */

#endif
