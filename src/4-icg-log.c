/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "4-icg.h"

extern char *inputpath;

#define IC_MNEMONIC__LIST_SIZE 26
#define IC_MNEMONIC__STR_SIZE 32
char IC_Mnemonic_List[IC_MNEMONIC__LIST_SIZE][IC_MNEMONIC__STR_SIZE] = {
    "ADD",
    "SUB",
    "MUL",
    "DIV",
    "CMP_GREATER",
    "CMP_LESS",
    "CMP_GREATER_EQUAL",
    "CMP_LESS_EQUAL",
    "LOAD",
    "STORE",
    "LOAD_IMMEDIATE",
    "LOAD_LOCAL",
    "STORE_LOCAL",
    "BRANCH",
    "BRANCH_EQUAL",
    "BRANCH_NOT_EQUAL",
    "BRANCH_IF_ONE",
    "BRANCH_IF_ZERO",
    "CALL",
    "RETURN",
    "RETURN_WITH_VALUE",
    "NOP",
    "ALLOC_STACK",
    "PUSH",
    "POP",
    "MOVE"};

char *get_mnemonic_name(enum IC_Instruction_Type opcode) {
  return IC_Mnemonic_List[opcode];
}

void print_ic_instruction_helper(char *dest_str, struct IC_Instruction *instr,
                                 char *format) {
  static char tmp[64];
  int i, j, m, k = 0;
  const int n = strlen(format);
  bool replace;
  char ch = '\0', last;
  for (i = 0; i < n; i++) {
    last = ch;
    ch = format[i];
    replace = false;
    switch (ch) {
    case '0':          /* operand 0 */
    case '1':          /* operand 1 */
    case '2':          /* operand 2 */
      if (last == '!') /* address */
        sprintf(tmp, "$%08lld", instr->op[ch - '0']);
      else
        sprintf(tmp, "%lld", instr->op[ch - '0']);
      replace = true;
      break;
    case '#': /* mnemonic */
      strcpy(tmp, get_mnemonic_name(instr->opc));
      replace = true;
      break;
    case 'I': /* symbol id */
      strcpy(tmp, instr->sym->id);
      replace = true;
      break;
    case '&': /* align */
      if (instr->align > 0) {
        sprintf(tmp, "[%d]", instr->align);
        replace = true;
      } else {
        continue;
      }
      break;
    case '!':
      continue;
    }
    if (replace) {
      m = strlen(tmp);
      for (j = 0; j < m; j++)
        dest_str[k++] = tmp[j];
    } else {
      dest_str[k++] = ch;
    }
  }
  dest_str[k] = '\0';
}

void print_ic_instruction(struct IC_Instruction *instr, int indent) {
  static char long_msg[1024];
  static char short_msg[1024];
  short_msg[0] = '\0';
  char operator[8];
  for (int i = 0; i < indent; i++)
    printf("    ");
  /* long message */
  switch (instr->opc) {
  case INSTR_ADD:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 @2 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := @1 + @2 &");
    break;
  case INSTR_SUB:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 @2 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := @1 - @2 &");
    break;
  case INSTR_MUL:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 @2 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := @1 * @2 &");
    break;
  case INSTR_DIV:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 @2 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := @1 / @2 &");
    break;
  case INSTR_CMP_GREATER:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 @2 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := @1 > @2 &");
    break;
  case INSTR_CMP_LESS:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 @2 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := @1 < @2 &");
    break;
  case INSTR_CMP_GREATER_EQUAL:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 @2 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := @1 == @2 &");
    break;
  case INSTR_CMP_LESS_EQUAL:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 @2 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := @1 <= @2 &");
    break;
  case INSTR_LOAD:
    print_ic_instruction_helper(long_msg, instr, "# @0 1 @!2 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := *(@1 + !2) &");
    break;
  case INSTR_STORE:
    print_ic_instruction_helper(long_msg, instr, "# @0 1 @!2 &");
    print_ic_instruction_helper(short_msg, instr, "*(@1 + !2) := @0 &");
    break;
  case INSTR_LOAD_IMMEDIATE:
    print_ic_instruction_helper(long_msg, instr, "# @0 1 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := 1 &");
    break;
  case INSTR_LOAD_LOCAL:
    if (instr->op[0] >= 0)
      print_ic_instruction_helper(long_msg, instr, "# @0 FP+1 &");
    else
      print_ic_instruction_helper(long_msg, instr, "# @0 FP1 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := I &");
    break;
  case INSTR_STORE_LOCAL:
    if (instr->op[0] >= 0)
      print_ic_instruction_helper(long_msg, instr, "# @0 FP+1 &");
    else
      print_ic_instruction_helper(long_msg, instr, "# @0 FP1 &");
    print_ic_instruction_helper(short_msg, instr, "I := @0 &");
    break;
  case INSTR_BRANCH:
    print_ic_instruction_helper(long_msg, instr, "# !2 &");
    print_ic_instruction_helper(short_msg, instr, "jump !2 &");
    break;
  case INSTR_BRANCH_EQUAL:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 !2  &");
    print_ic_instruction_helper(short_msg, instr, "if(@0==@1) { jump !2; } &");
    break;
  case INSTR_BRANCH_NOT_EQUAL:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 !2  &");
    print_ic_instruction_helper(short_msg, instr, "if(@0!=@1) { jump !2; } &");
    break;
  case INSTR_BRANCH_IF_ONE:
    print_ic_instruction_helper(long_msg, instr, "# @0 !2  &");
    print_ic_instruction_helper(short_msg, instr, "if(@0==1) { jump !2; } &");
    break;
  case INSTR_BRANCH_IF_ZERO:
    print_ic_instruction_helper(long_msg, instr, "# @0 !2  &");
    print_ic_instruction_helper(short_msg, instr, "if(@0==0) { jump !2; } &");
    break;
  case INSTR_CALL:
    print_ic_instruction_helper(long_msg, instr, "# !2 &");
    print_ic_instruction_helper(short_msg, instr, "call I &");
    break;
  case INSTR_RETURN:
    print_ic_instruction_helper(long_msg, instr, "# &");
    print_ic_instruction_helper(short_msg, instr, "return &");
    break;
  case INSTR_RETURN_WITH_VALUE:
    print_ic_instruction_helper(long_msg, instr, "# &");
    print_ic_instruction_helper(short_msg, instr, "return @0 &");
    break;
  case INSTR_NOP:
    print_ic_instruction_helper(long_msg, instr, "# &");
    print_ic_instruction_helper(short_msg, instr, "nop &");
    break;
  case INSTR_ALLOC_STACK:
    print_ic_instruction_helper(long_msg, instr, "# 0 &");
    print_ic_instruction_helper(short_msg, instr, "alloc(0) &");
    break;
  case INSTR_POP:
    print_ic_instruction_helper(long_msg, instr, "# 0 &");
    print_ic_instruction_helper(short_msg, instr, "pop(0) &");
    break;
  case INSTR_PUSH:
    print_ic_instruction_helper(long_msg, instr, "# @0 &");
    print_ic_instruction_helper(short_msg, instr, "push(@0) &");
    break;
  case INSTR_MOVE:
    print_ic_instruction_helper(long_msg, instr, "# @0 @1 &");
    print_ic_instruction_helper(short_msg, instr, "@0 := @1 &");
    break;
  default:
    ASSERT(false); /* unimplemented */
  }

  printf("$%08d %-32s # %-32s  %s:%d\n", instr->addr, long_msg, short_msg,
         inputpath, instr->sym->src_line);
}
