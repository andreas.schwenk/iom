/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <string.h>

#include "lib.h"

#include "2-ast.h"

extern int yylineno;

struct Symbol *get_symbol(struct Symbol_Table *leaf, char *var_id) {
  struct Symbol_Table *sym_table = leaf;
  struct Symbol *sym;
  do {
    sym = mate__get_hashtable_item(sym_table->hashtable, var_id);
    if (sym != NULL)
      return sym;
    sym_table = sym_table->parent;
  } while (sym_table != NULL);
  return NULL;
}

struct Symbol *create_symbol(char *id, int type, struct Symbol *ast_parent,
                             struct Symbol_Table *sym_table) {
  struct Symbol *sym = ALLOC(struct Symbol, 1);
  sym->id = strdup_(id);
  sym->type = type;
  sym->datatype = TYPE_UNDEFINED;
  sym->memory_offset = 0;
  sym->memory_size = 0;
  sym->src_line = yylineno;
  sym->ast_parent = ast_parent;
  sym->ast_children = mate__create_list();
  sym->intermediate_code = mate__create_list();
  if (sym->ast_parent != NULL)
    mate__push_to_list(sym->ast_parent->ast_children, sym);
  sym->sym_table = sym_table;
  // TODO: CHECK, if "id" is already in symbol-table!!!!!
  return sym;
}
