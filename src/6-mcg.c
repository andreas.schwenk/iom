/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include "config.h"

#include "6-mcg-iomcpu.h"

#include "6-mcg.h"

enum BACKEND_ARCHITECTURE architecture =
    BACKEND_ARCHITECTURE_IOM_CPU; // TODO: add to command line options

extern struct Symbol *ast;

void generate_machine_code(struct Symbol *sym) {
  switch (architecture) {
  case BACKEND_ARCHITECTURE_IOM_CPU:
    //generate_machine_code__iom_cpu(sym); // TODO
    break;
  default:
    assert(false); /* unimplemented */
  }
}

void print_machine_code(struct Symbol *sym, int indent) {
  switch (architecture) {
  case BACKEND_ARCHITECTURE_IOM_CPU:
    print_machine_code__iom_cpu(sym, indent);
    break;
  default:
    assert(false); /* unimplemented */
  }
}

void write_machine_code(void) {
  switch (architecture) {
  case BACKEND_ARCHITECTURE_IOM_CPU:
    write_machine_code__iom_cpu(ast);
    break;
  default:
    assert(false); /* unimplemented */
  }
}
