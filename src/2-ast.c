/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "3-sa.h"
#include "4-icg.h"
#include "config.h"

#include "2-ast.h"

extern int yylineno;
extern void yyerror(const char *s);

struct Symbol *ast = NULL; /* abstract syntax tree */

struct mate__List *exp_stack; /* expression stack */

struct Symbol *current_function = NULL;
struct Symbol *current_function_call = NULL;

enum Type current_type;

struct mate__List *block_stack; /* function, if-statement, loop, ... */

char tmp_str_1[2048], tmp_str_2[2048];

void init_parser(void) {
  ast = create_symbol("root", SYM_ROOT, NULL, create_symbol_table(NULL));
  exp_stack = mate__create_list();
  block_stack = mate__create_list();
  mate__push_to_list(block_stack, ast);
}

void release_parser(void) {
  mate__delete_list(exp_stack, false, NULL);
  mate__delete_list(block_stack, false, NULL);
  // TODO: delete AST recursively (needed???)
}

void push_type(enum Type type) { current_type = type; }

void push_module(char *id) {
  if (mate__get_list_length(top_of_block_stack()->ast_children) > 0)
    yyerror("not allowed to define 'module' here.");
  struct Symbol *sym = create_symbol(id, SYM_MODULE, top_of_block_stack(),
                                     top_of_block_stack()->sym_table);
}
