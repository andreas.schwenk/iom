/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include "3-sa.h"

#include "2-ast.h"

extern struct mate__List *block_stack; /* function, if-statement, loop, ... */

extern struct mate__List *exp_stack; /* expression stack */

void begin_for_loop(char *loop_var) {
  struct Symbol_Table *old_sym_table = top_of_block_stack()->sym_table;
  struct Symbol_Table *new_sym_table = create_symbol_table(old_sym_table);
  struct Symbol *sym_for =
      create_symbol("for", SYM_FOR_LOOP, top_of_block_stack(), new_sym_table);
  struct Symbol *sym_loop_var =
      create_symbol(loop_var, SYM_LOCAL_VARIABLE, sym_for, new_sym_table);
  sym_loop_var->datatype = TYPE_INT;
  sym_loop_var->memory_size = get_memory_size(sym_loop_var);
  add_to_symbol_table(new_sym_table, sym_loop_var);
  ASSERT(mate__get_list_length(exp_stack) == 2);
  struct Symbol *upper = mate__pop_from_list(exp_stack);
  struct Symbol *lower = mate__pop_from_list(exp_stack);
  mate__push_to_list(sym_for->ast_children, lower);
  mate__push_to_list(sym_for->ast_children, upper);
  mate__push_to_list(block_stack, sym_for);
  // TODO: typecheck
}

void end_for_loop() { mate__pop_from_list(block_stack); }
