/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>

#include <mate/list.h>

#include "4-icg.h"
#include "6-mcg.h"

#include "5-opt.h"

struct Basicblock *create_basicblock(struct Symbol *sym) {
  struct Basicblock *bb = ALLOC(struct Basicblock, 1);
  bb->intermediate_code = mate__create_list();
  bb->machine_code = mate__create_list();
  mate__push_to_list(sym->code_basicblocks, bb);
  return bb;
}

void optimize_intermediate_code(struct Symbol *sym) {
  sym->code_basicblocks = mate__create_list();

  struct Basicblock *current_basicblock = create_basicblock(sym);

  struct mate__List_iterator iterator =
      mate__create_list_iterator(sym->intermediate_code);
  struct IC_Instruction *instr;
  while ((instr = mate__get_next_list_iterator_item(&iterator)) != NULL) {
    if (instr->is_jump_dest)
      current_basicblock = create_basicblock(sym);
    mate__push_to_list(current_basicblock->intermediate_code, instr);
    instr->basicblock = current_basicblock;
    switch (instr->opc) {
    case INSTR_BRANCH:
    case INSTR_BRANCH_EQUAL:
    case INSTR_BRANCH_NOT_EQUAL:
    case INSTR_BRANCH_IF_ONE:
    case INSTR_BRANCH_IF_ZERO:
    case INSTR_CALL:
      current_basicblock = create_basicblock(sym);
      break;
    }
  }

  generate_machine_code(sym);
}
