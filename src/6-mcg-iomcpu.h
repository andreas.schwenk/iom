/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== MCG := MACHINE CODE GENERATION ===================================== */

#ifndef __MCG_IOM__H
#define __MCG_IOM__H

#include <inttypes.h>

#include <mate/common.h>

#include "6-mcg.h"

/* ----- machine code generation for the IOM CPU ---------------------------- */

/* --- register set:
   - 32 registers reg[0] .. reg[31] (width: each 64 bit)
   - special purpose registers: RR, FP, SP, PC (see below)
   --- memory:
   - mem[ x ] := memory at address x
*/

#define IOMCPU_REG__RETURN_VALUE 28    /* RR := return register */
#define IOMCPU_REG__FRAME_POINTER 29   /* FP := frame pointer */
#define IOMCPU_REG__STACK_POINTER 30   /* SP := stack pointer */
#define IOMCPU_REG__PROGRAM_COUNTER 31 /* PC := program counter */

// TODO
enum IOMCPU_Opcode {
  /* corresponds to IC_Mnemonic_List in *-log.c - file */

  /* ----- 0-15: instruction type A -----
     instruction layout: (64 bit width):
      63 .. 58: opcode
      57 ..  0: value    (operand 0 =: op0)
  */
  IOMCPU_OPC_SHUTDOWN = 0,
  IOMCPU_OPC_NO_OP = 1, /* no operation */
  IOMCPU_OPC_JUMP = 2,  /* reg[PC] := op0 */
  // TODO: keep CPU simple: this is a move instruction!
  IOMCPU_OPC_CALL = 3,   /*
// TODO: keep CPU simple: must generate the following:
    mem[ reg[SP] ] := reg[PC]; reg[SP] ++;   # push PC
    mem[ reg[SP] ] := reg[FP]; reg[SP] --;   # push FP
    reg[FP] := reg[SP];                      # FP := SP
    reg[PC] := op0;                          # jump op0
  */
  IOMCPU_OPC_RETURN = 4, /*
// TODO: keep CPU simple: must generate the following:
    reg[SP] := reg[FP];              # SP := FP
    reg[FP] := mem[ reg[SP] - 1 ];   # mem[FP] := mem[SP-1]
    reg[SP] := reg[SP] + 2;          # SP += 2;  (pop PC and FP)
    reg[PC] := reg[SP];              # jump mem[SP]
  */

  /* ----- 16-31: instruction type B -----
     instruction layout: (64 bit width):
      63 .. 58: opcode
      57 .. 53: reg0     (operand 0 =: op0)
      52 ..  0: value    (operand 1 =: op1)
  */
  IOMCPU_OPC_LOAD = 16,              /* reg[op0] := mem[ op1 ] */
  IOMCPU_OPC_STORE = 17,             /* mem[ op1 ] := reg[op0] */
  IOMCPU_OPC_LOAD_UPPER_IMM = 18,    /* reg[op0] := reg[op0] | (op1 << 53) */
  IOMCPU_OPC_LOAD_IMMEDIATE = 19,    /* reg[op0] := op1 */
  IOMCPU_OPC_RETURN_WITH_VALUE = 20, /*
  TODO: keep CPU simple: must generate the following
    reg[RR] := reg[op0];
    IOMCPU_OPC_RETURN;
  */

  /* ----- 32-47: instruction type C -----
     instruction layout: (64 bit width):
      63 .. 58: opcode
      57 .. 53: reg0     (operand 0 =: op0)
      52 .. 48: reg1     (operand 1 =: op1)
      47 ..  0: value    (operand 2 =: op2)
  */
  IOMCPU_OPC_MOVE = 32,         /* reg[op0] := reg[op1] */
  IOMCPU_OPC_LOAD_OFFSET = 33,  /* reg[op0] := mem[ reg[op1] + op2 ] */
  IOMCPU_OPC_STORE_OFFSET = 34, /* mem[ reg[op1] + op2 ] := reg[op0] */
  IOMCPU_OPC_IS_POSITIVE = 35,  /* reg[op0] := reg[op1] >= 0 ? 1 : 0 */

  /* ----- 48-63: instruction type D -----
     instruction layout: (64 bit width):
      63 .. 58: opcode
      57 .. 53: reg0     (operand 0 =: op0)
      52 .. 48: reg1     (operand 1 =: op1)
      47 .. 43: reg2     (operand 2 =: op2)
      42 ..  0: value    (unused)
  */
  IOMCPU_OPC_ADD = 48,       /* reg[op0] := reg[op1] + reg[op2] */
  IOMCPU_OPC_SUB = 49,       /* reg[op0] := reg[op1] - reg[op2] */
  IOMCPU_OPC_MUL = 50,       /* reg[op0] := reg[op1] * reg[op2] */
  IOMCPU_OPC_DIV = 51,       /* reg[op0] := reg[op1] / reg[op2] */
  IOMCPU_OPC_CMP = 52,       /*
          if(reg[op1] == reg[op2])
            reg[op0] := 0;
          else if(reg[op1] > reg[op2])
            reg[op0] := 1;
          else
            reg[op0] := -1;
        */
  IOMCPU_OPC_FLOAT_ADD = 53, /* reg[op0] := float(reg[op1]) + float(reg[op2]) */
  IOMCPU_OPC_FLOAT_SUB = 54, /* reg[op0] := float(reg[op1]) - float(reg[op2]) */
  IOMCPU_OPC_FLOAT_MUL = 55, /* reg[op0] := float(reg[op1]) * float(reg[op2]) */
  IOMCPU_OPC_FLOAT_DIV = 56  /* reg[op0] := float(reg[op1]) / float(reg[op2]) */
};

// TODO
struct IOMCPU_Instruction {
  uint64_t address;
  enum IOMCPU_Opcode opcode;
  int64_t op[3];                      /* operands */
  struct Basicblock *dest_basicblock; /* e.g. jump target */
  uint64_t encoded;                   /* final machine code */
};

// TODO
void iomcpu_encode_instruction(struct IOMCPU_Instruction *instr);

// TODO
void iomcpu_decode_instruction(struct IOMCPU_Instruction *dest, uint64_t code);

// TODO
void generate_machine_code__iom_cpu(struct Symbol *sym);

// TODO
void print_machine_code__iom_cpu(struct Symbol *sym, int indent);

// TODO
void write_machine_code__iom_cpu(struct Symbol *ast_root);

// TODO
char *get_iomcpu_mnemonic_name(enum IOMCPU_Opcode opcode);

// TODO
void print_mc_instruction(struct IOMCPU_Instruction *instr, int indent);

/* -------------------------------------------------------------------------- */

#endif
