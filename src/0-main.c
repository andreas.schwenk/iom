/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include "2-ast.h"
#include "6-mcg.h"

extern int yylex();
extern int yyparse();

extern FILE *yyin; /* input source file */

FILE *objfile;

char *inputpath = NULL;
char *outputpath = NULL;

void print_usage(void) { fprintf(stderr, "Usage: iom-c src_file\n"); }

int main(int argc, char *argv[]) {
  printf("IOM-COMPILER -- Copyright 2018 Andreas Schwenk - GPL 3 LICENSE\n");

  int opt;
  char mode = ' ';

  while ((opt = getopt(argc, argv, "h")) != -1) {
    switch (opt) {
    /*case 'h':
      mode = 'h';
      break;*/
    default:
      print_usage();
      exit(EXIT_FAILURE);
    }
  }

  inputpath = argv[optind];

  outputpath = "a.out";

  yyin = fopen(inputpath, "r");
  if (yyin == NULL) {
    fprintf(stderr, "Error: invalid input path '%s'\n", inputpath);
    print_usage();
    exit(EXIT_FAILURE);
  }

  objfile = fopen(outputpath, "wb");
  if (objfile == NULL) {
    fprintf(stderr, "Error: invalid output path '%s'\n", outputpath);
    print_usage();
    exit(EXIT_FAILURE);
  }

  init_parser();

  printf("---------- TOKEN LIST ----------\n");
  yyparse();

  print_abstract_syntax_tree();

  write_machine_code();

  fclose(yyin);

  release_parser();

  exit(EXIT_SUCCESS);
}
