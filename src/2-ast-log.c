/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "4-icg.h"
#include "5-opt.h"
#include "6-mcg.h"

#include "2-ast.h"

extern struct Symbol *ast; /* abstract syntax tree */

char *get_datatype_name(enum Type type) {
  static char tmp[2048];
  switch (type) {
  case TYPE_UNDEFINED:
    strcpy(tmp, "TYPE_UNDEFINED");
    break;
  case TYPE_BOOL:
    strcpy(tmp, "TYPE_BOOL");
    break;
  case TYPE_INT:
    strcpy(tmp, "TYPE_INT");
    break;
  case TYPE_REAL:
    strcpy(tmp, "TYPE_REAL");
    break;
  default:
    ASSERT(false); /* unimplemented */
  }
  return tmp;
}

char *get_symbol_type_name(enum Symbol_Type type) {
  static char tmp[2048];
  switch (type) {
  case SYM_ROOT:
    strcpy(tmp, "SYM_ROOT");
    break;
  case SYM_FUNCTION:
    strcpy(tmp, "SYM_FUNCTION");
    break;
  case SYM_FUNCTION_PARAM:
    strcpy(tmp, "SYM_FUNCTION_PARAM");
    break;
  case SYM_BEGIN:
    strcpy(tmp, "SYM_BEGIN");
    break;
  case SYM_CONST_INTEGER:
    strcpy(tmp, "SYM_CONST_INTEGER");
    break;
  case SYM_CONST_REAL:
    strcpy(tmp, "SYM_CONST_REAL");
    break;
  case SYM_LOCAL_VARIABLE:
    strcpy(tmp, "SYM_LOCAL_VARIABLE");
    break;
  case SYM_ASSIGNMENT:
    strcpy(tmp, "SYM_ASSIGNMENT");
    break;
  case SYM_ADD_ASSIGNMENT:
    strcpy(tmp, "SYM_ADD_ASSIGNMENT");
    break;
  case SYM_SUB_ASSIGNMENT:
    strcpy(tmp, "SYM_SUB_ASSIGNMENT");
    break;
  case SYM_MUL_ASSIGNMENT:
    strcpy(tmp, "SYM_MUL_ASSIGNMENT");
    break;
  case SYM_DIV_ASSIGNMENT:
    strcpy(tmp, "SYM_DIV_ASSIGNMENT");
    break;
  case SYM_EQUAL:
    strcpy(tmp, "SYM_EQUAL");
    break;
  case SYM_LESS:
    strcpy(tmp, "SYM_LESS");
    break;
  case SYM_GREATER:
    strcpy(tmp, "SYM_GREATER");
    break;
  case SYM_LESS_EQUAL:
    strcpy(tmp, "SYM_LESS_EQUAL");
    break;
  case SYM_GREATER_EQUAL:
    strcpy(tmp, "SYM_GREATER_EQUAL");
    break;
  case SYM_ADD:
    strcpy(tmp, "SYM_ADD");
    break;
  case SYM_SUBTRACT:
    strcpy(tmp, "SYM_SUBTRACT");
    break;
  case SYM_MULTIPLY:
    strcpy(tmp, "SYM_MULTIPLY");
    break;
  case SYM_DIVIDE:
    strcpy(tmp, "SYM_DIVIDE");
    break;
  case SYM_FOR_LOOP:
    strcpy(tmp, "SYM_FOR_LOOP");
    break;
  case SYM_FUNCTION_CALL:
    strcpy(tmp, "SYM_FUNCTION_CALL");
    break;
  case SYM_RETURN:
    strcpy(tmp, "SYM_RETURN");
    break;
  case SYM_MODULE:
    strcpy(tmp, "SYM_MODULE");
    break;
  case SYM_BLOCK:
    strcpy(tmp, "SYM_BLOCK");
    break;
  case SYM_IF_STATEMENT:
    strcpy(tmp, "SYM_IF_STATEMENT");
    break;
  default:
    ASSERT(false); /* unimplemented */
  }
  return tmp;
}

#define PRINT_INDENT(N)                                                        \
  {                                                                            \
    for (int i = 0; i < (N); i++)                                              \
      printf("    ");                                                          \
  }

void print_abstract_syntax_tree__rec(struct Symbol *sym, int indent) {
  static char additional[256]; // TODO: large enough?
  additional[0] = '\0';

  switch (sym->type) {
  case SYM_ROOT:
    break;
  case SYM_CONST_INTEGER:
    sprintf(additional, " value='%d'", sym->value_int);
    break;
  case SYM_CONST_REAL:
    sprintf(additional, " value='%f'", sym->value_real);
    break;
  }

  /* print symbol */
  PRINT_INDENT(indent);
  if (strlen(sym->id) > 0)
    printf("%s: id='%s'%s", get_symbol_type_name(sym->type), sym->id,
           additional);
  else
    printf("%s:%s", get_symbol_type_name(sym->type), additional);
  if (sym->type == SYM_LOCAL_VARIABLE || sym->type == SYM_FUNCTION ||
      sym->type == SYM_FUNCTION_PARAM || sym->type == SYM_CONST_INTEGER ||
      sym->type == SYM_CONST_REAL)
    printf(" : %s", get_datatype_name(sym->datatype));
  if (sym->type == SYM_LOCAL_VARIABLE)
    printf(" (@sym-table %d)", sym->sym_table->idx);
  printf("\n");

  /* print symbol table */
  if (sym->type == SYM_ROOT || sym->type == SYM_FUNCTION ||
      sym->type == SYM_IF_STATEMENT ||
      sym->type ==
          SYM_FOR_LOOP) // TODO: SYM_FUNCTION | SYM_FOR_LOOP | ... -> create
                        // function or definition ~"is_anchor(..)"
    print_symbol_table(sym->sym_table, indent);

  /* print recursively */
  struct mate__List_iterator iterator =
      mate__create_list_iterator(sym->ast_children);
  struct Symbol *child;
  while ((child = mate__get_next_list_iterator_item(&iterator)) != NULL)
    print_abstract_syntax_tree__rec(child, indent + 1);

  /* print intermediate and machine code */
  if ((sym->type == SYM_FUNCTION) &&
      mate__get_list_length(sym->intermediate_code) > 0) {
    struct IC_Instruction *instr;
    struct mate__List_iterator ic_iterator;

    /* print intermediate code (basicblocks) */
    PRINT_INDENT(indent);
    printf("*** (list of intermediate code basicblocks:)\n");
    struct mate__List_iterator basicblock_iterator =
        mate__create_list_iterator(sym->code_basicblocks);
    struct Basicblock *bb;
    while ((bb = mate__get_next_list_iterator_item(&basicblock_iterator)) !=
           NULL) {
      ic_iterator = mate__create_list_iterator(bb->intermediate_code);
      while ((instr = mate__get_next_list_iterator_item(&ic_iterator)) != NULL)
        print_ic_instruction(instr, indent);
      PRINT_INDENT(indent);
      printf("***\n");
    }
    PRINT_INDENT(indent);
    printf("--------------------------------\n");

    /* print machine code */
    PRINT_INDENT(indent);
    printf("*** (list of machine code basicblocks:)\n");
    print_machine_code(sym, indent);
    PRINT_INDENT(indent);
    printf("--------------------------------\n");
  }
}

void print_abstract_syntax_tree(void) {
  printf("---------- ABSTRACT SYNTAX TREE ----------\n");
  ASSERT(ast != NULL);
  print_abstract_syntax_tree__rec(ast, 0);
}

void print_symbol_table(struct Symbol_Table *symbol_table, int indent) {
  PRINT_INDENT(indent);
  printf("--------------------------------\n");
  PRINT_INDENT(indent);
  printf("| SYMBOL-TABLE %d: (parent=%d children={", symbol_table->idx,
         symbol_table->parent == NULL ? -1 : symbol_table->parent->idx);
  struct mate__List_iterator children_iterator =
      mate__create_list_iterator(symbol_table->children);
  struct Symbol_Table *child_table;
  while ((child_table =
              mate__get_next_list_iterator_item(&children_iterator)) != NULL) {
    printf("%d ", child_table->idx);
  }
  printf("})\n");
  struct Symbol *sym;
  struct mate__List_iterator it =
      mate__create_list_iterator(symbol_table->list);
  while ((sym = mate__get_next_list_iterator_item(&it)) != NULL) {
    PRINT_INDENT(indent);
    printf("|  %s : %s, %s, location=%d\n", sym->id,
           get_symbol_type_name(sym->type), get_datatype_name(sym->datatype),
           sym->memory_offset);
  }
  PRINT_INDENT(indent);
  printf("--------------------------------\n");
}
