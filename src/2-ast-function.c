/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include "4-icg.h"

#include "2-ast.h"

extern struct mate__List *exp_stack; /* expression stack */

extern struct mate__List *block_stack; /* function, if-statement, loop, ... */

extern enum Type current_type;

extern int yylineno;
extern void yyerror(const char *s);

extern struct Symbol *current_function;
extern struct Symbol *current_function_call;

void begin_function(char *id) {
  struct Symbol_Table *old_sym_table = top_of_block_stack()->sym_table;
  struct Symbol_Table *new_sym_table = create_symbol_table(old_sym_table);
  struct Symbol *sym_fct =
      create_symbol(id, SYM_FUNCTION, top_of_block_stack(), new_sym_table);
  current_function = sym_fct;
  add_to_symbol_table(old_sym_table, sym_fct);
  mate__push_to_list(block_stack, sym_fct);
}

void end_of_function_parameter_declaration(void) {
  struct Symbol *sym_fct_begin = create_symbol(
      "", SYM_BEGIN, top_of_block_stack(), top_of_block_stack()->sym_table);
}

void end_function(void) {
  generate_intermediate_code(top_of_block_stack());
  mate__pop_from_list(block_stack);
  current_function = NULL;
}

void push_function_return_type(void) {
  current_function->datatype = current_type;
}

void push_function_parameter(char *id) {
  struct Symbol *sym =
      create_symbol(id, SYM_FUNCTION_PARAM, top_of_block_stack(),
                    top_of_block_stack()->sym_table);
  sym->datatype = current_type;
  // mate__insert_into_hashtable(sym->sym_table->symbols, sym->id, sym); //
  // TODO: remove
  add_to_symbol_table(sym->sym_table, sym);
}

void push_return(bool has_value) {
  struct Symbol *sym_return =
      create_symbol("return", SYM_RETURN, top_of_block_stack(),
                    top_of_block_stack()->sym_table);
  if (has_value) {
    ASSERT(mate__get_list_length(exp_stack) == 1);
    struct Symbol *sym_exp = mate__pop_from_list(exp_stack);
    sym_exp->ast_parent = sym_return;
    mate__push_to_list(sym_return->ast_children, sym_exp);
  }
}
