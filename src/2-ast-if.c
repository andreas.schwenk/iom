/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include "2-ast.h"

extern struct mate__List *block_stack; /* function, if-statement, loop, ... */

extern struct mate__List *exp_stack; /* expression stack */

void begin_if_statement(void) {
  struct Symbol_Table *old_sym_table = top_of_block_stack()->sym_table;
  struct Symbol_Table *new_sym_table = create_symbol_table(old_sym_table);
  struct Symbol *sym_if = create_symbol("if", SYM_IF_STATEMENT,
                                        top_of_block_stack(), new_sym_table);
  ASSERT(mate__get_list_length(exp_stack) == 1);
  struct Symbol *sym_exp = mate__pop_from_list(exp_stack);
  sym_exp->ast_parent = sym_if;
  mate__push_to_list(sym_if->ast_children, sym_exp);
  mate__push_to_list(block_stack, sym_if);
  // TODO: typecheck
}

void end_if_statement() { mate__pop_from_list(block_stack); }
