/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "3-sa.h"

#include "2-ast.h"

extern void yyerror(const char *s);

extern struct Symbol *ast; /* abstract syntax tree */

extern struct mate__List *exp_stack; /* expression stack */

extern struct Symbol *current_function;
extern struct Symbol *current_function_call;

extern struct mate__List *block_stack;

extern char tmp_str_1[2048], tmp_str_2[2048];

void push_const_int(int value) {
  sprintf(tmp_str_1, "%d", value);
  struct Symbol *sym = create_symbol(tmp_str_1, SYM_CONST_INTEGER, NULL,
                                     top_of_block_stack()->sym_table);
  sym->value_int = value;
  sym->datatype = TYPE_INT;
  sym->memory_size = get_memory_size(sym);
  mate__push_to_list(exp_stack, sym);
}

void push_const_real(double value) {
  sprintf(tmp_str_1, "%f", value);
  struct Symbol *sym = create_symbol(tmp_str_1, SYM_CONST_REAL, NULL,
                                     top_of_block_stack()->sym_table);
  sym->value_real = value;
  sym->datatype = TYPE_REAL;
  sym->memory_size = get_memory_size(sym);
  mate__push_to_list(exp_stack, sym);
}

void push_variable(char *id) {
  struct Symbol *sym = get_symbol(top_of_block_stack()->sym_table, id);
  if (sym == NULL)
    sym = create_symbol(id, SYM_LOCAL_VARIABLE, NULL,
                        top_of_block_stack()->sym_table);
  else if (sym->type != SYM_LOCAL_VARIABLE && sym->type != SYM_FUNCTION_PARAM) {
    sprintf(tmp_str_1, "'%s' is not a variable.", id);
    yyerror(tmp_str_1);
  }
  mate__push_to_list(exp_stack, sym);
}

void push_function_call(char *id) {
  struct Symbol *sym_call = create_symbol(id, SYM_FUNCTION_CALL, NULL,
                                          top_of_block_stack()->sym_table);
  current_function_call = sym_call;
  struct Symbol *sym_function =
      mate__get_hashtable_item(ast->sym_table->hashtable, sym_call->id);
  if (sym_function == NULL) {
    sprintf(tmp_str_1, "unknown function %s", sym_call->id);
    yyerror(tmp_str_1);
  }
  sym_call->datatype = sym_function->datatype;

  // TODO: semantic analysis: check parameters!
  mate__push_to_list(exp_stack, sym_call);
}

void push_function_argument(void) {
  ASSERT(mate__get_list_length(exp_stack) > 0);
  struct Symbol *sym = mate__pop_from_list(exp_stack);
  sym->ast_parent = current_function_call;
  mate__push_to_list(current_function_call->ast_children, sym);
}

void push_binary_operation(char *op) {
  ASSERT(mate__get_list_length(exp_stack) >= 2);
  int sym_type;
  const int op_len = strlen(op);
  ASSERT(op_len >= 1);
  switch (op[0]) {
  case '=':
    sym_type = op_len == 2 /* '=='*/ ? SYM_EQUAL : SYM_ASSIGNMENT;
    break;
  case '+':
    sym_type = op_len == 2 /* '+='*/ ? SYM_ADD_ASSIGNMENT : SYM_ADD;
    break;
  case '-':
    sym_type = op_len == 2 /* '-='*/ ? SYM_SUB_ASSIGNMENT : SYM_SUBTRACT;
    break;
  case '*':
    sym_type = op_len == 2 /* '*='*/ ? SYM_MUL_ASSIGNMENT : SYM_MULTIPLY;
    break;
  case '/':
    sym_type = op_len == 2 /* '/='*/ ? SYM_DIV_ASSIGNMENT : SYM_DIVIDE;
    break;
  case '<':
    sym_type = op_len == 2 /* '<='*/ ? SYM_LESS_EQUAL : SYM_LESS;
    break;
  case '>':
    sym_type = op_len == 2 /* '>='*/ ? SYM_GREATER_EQUAL : SYM_GREATER;
    break;
  default:
    ASSERT(false);
  }
  struct Symbol *sym =
      create_symbol(op, sym_type, NULL, top_of_block_stack()->sym_table);
  struct Symbol *rhs = mate__pop_from_list(exp_stack);
  struct Symbol *lhs = mate__pop_from_list(exp_stack);
  lhs->ast_parent = sym;
  rhs->ast_parent = sym;
  mate__push_to_list(sym->ast_children, lhs);
  mate__push_to_list(sym->ast_children, rhs);
  mate__push_to_list(exp_stack, sym);
  typecheck_binary_operation(sym);
}

void push_expression(void) {
  ASSERT(mate__get_list_length(exp_stack) == 1);
  struct Symbol *sym_exp = mate__pop_from_list(exp_stack);
  sym_exp->ast_parent = top_of_block_stack();
  mate__push_to_list(top_of_block_stack()->ast_children, sym_exp);
}
