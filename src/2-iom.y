/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

%{
  #include <stdlib.h>
  #include <stdio.h>
  #include <stdbool.h>

  #include "../src/2-ast.h"

  extern int yylineno;

  extern int yylex();
  extern int yyparse();
  extern FILE *yyin;

  extern char *inputpath;
  void yyerror(const char *s);

  // TODO:
  #define YYDEBUG 1
  #define YYERROR_VERBOSE 1

  char assignment_op;
%}

%locations    /* enables e.g. "yylineno" */

%error-verbose

%union {
  int     int_val;
  double  real_val;
  char   *string_val;
}

/* ----- TERMINAL SYMBOLS --------------------------------------------------- */

%token TK_SPACE TK_TABULTOR
%token TK_LEFT_PARENTHESIS TK_RIGHT_PARENTHESIS
%token TK_PLUS TK_MINUS TK_ASTERISK TK_DIVIDE
%token TK_EQUAL TK_LESS TK_GREATER TK_LEQ TK_GEQ
%token TK_COLON TK_COMMA TK_SEMICOLON
%token TK_LEFT_BRACE TK_RIGHT_BRACE
%token TK_LEFT_BRACKET TK_RIGHT_BRACKET
%token TK_ASSIGN TK_ADD_ASSIGN TK_SUB_ASSIGN TK_MUL_ASSIGN TK_DIV_ASSIGN
%token TK_UNEXPECTED

%token TK_MODULE TK_RETURN TK_FOR TK_IF TK_IN TK_INT TK_REAL

/* ----- NONTERMINALS SYMBOLS ----------------------------------------------- */

%token <int_val>    TK_CONST_INT
%token <real_val> TK_CONST_REAL
%token <string_val> TK_IDENTIFIER

/* TODO: operator precedence */

/* ----- RULES -------------------------------------------------------------- */

%start program

%%

function_argument:
  expression                   { push_function_argument(); }
  ;

function_argument_list:
  function_argument
  | function_argument_list
    TK_COMMA
    function_argument
  | /* no argument */
  ;

unary:
  TK_IDENTIFIER                { push_variable($1); }
  | TK_IDENTIFIER              { push_function_call($1); }
    TK_LEFT_PARENTHESIS
    function_argument_list
    TK_RIGHT_PARENTHESIS
  | TK_CONST_INT               { push_const_int($1); }
  | TK_CONST_REAL              { push_const_real($1); }
  | TK_LEFT_PARENTHESIS
    expression
    TK_RIGHT_PARENTHESIS
  ;

mul:
  unary
  | mul TK_ASTERISK unary      { push_binary_operation("*"); }
  | mul TK_DIVIDE   unary      { push_binary_operation("/"); }
  ;

add:
  mul
  | add TK_PLUS  mul           { push_binary_operation("+"); }
  | add TK_MINUS mul           { push_binary_operation("-"); }
  ;

relational:
  add
  | relational TK_EQUAL add    { push_binary_operation("==");  }
  | relational TK_LESS add     { push_binary_operation("<");  }
  | relational TK_GREATER add  { push_binary_operation(">");  }
  | relational TK_LEQ add      { push_binary_operation("<="); }
  | relational TK_GEQ add      { push_binary_operation(">="); }
  ;

expression:
  relational
  | expression TK_ASSIGN     relational   { push_binary_operation("="); }
  | expression TK_ADD_ASSIGN relational   { push_binary_operation("+="); }
  | expression TK_SUB_ASSIGN relational   { push_binary_operation("-="); }
  | expression TK_MUL_ASSIGN relational   { push_binary_operation("*="); }
  | expression TK_DIV_ASSIGN relational   { push_binary_operation("/="); }
  ;

return:
  TK_RETURN                    { push_return(false); }
  | TK_RETURN expression       { push_return(true); }
  ;

for_loop_range:
  TK_LEFT_BRACKET
  expression                   { /*push_lower_range(); push_expression();*/ }
  TK_COMMA
  expression                   { /*push_upper_range(); push_expression();*/ }
  TK_RIGHT_BRACKET
  ;

for_loop:
  TK_FOR
  TK_IDENTIFIER
  TK_IN
  for_loop_range               { begin_for_loop($2); }
  block                        { end_if_statement(); }
  ;

if:
  TK_IF
  TK_LEFT_PARENTHESIS
  expression
  TK_RIGHT_PARENTHESIS         { begin_if_statement(); }
  block                        { end_if_statement(); }
  ;

statement_end:
  TK_SEMICOLON
  ;

statement:
  expression statement_end     { push_expression(); }
  | for_loop
  | if
  | return statement_end
  ;

statement_list:
  statement
  | statement_list statement
  ;

block:
  TK_LEFT_BRACE                { /*push_block();*/ }
  statement_list
  TK_RIGHT_BRACE               { /*end_block();*/ }
  |                            { /*push_block();*/ }
  statement                    { /*end_block();*/ }
  ;

function_visibility:
  TK_PLUS
  | TK_MINUS
  ;

type:
  TK_INT                       { push_type(TYPE_INT); }
  | TK_REAL                    { push_type(TYPE_REAL); }
  ;

function_parameter:
  TK_IDENTIFIER
  TK_COLON
  type                         { push_function_parameter($1); }
  ;

function_parameter_list:
  function_parameter
  | function_parameter_list
    TK_COMMA
    function_parameter
  | /* no parameter */
  ;

function:
  function_visibility
  TK_IDENTIFIER                { begin_function($2); }
  TK_LEFT_PARENTHESIS
  function_parameter_list
  TK_RIGHT_PARENTHESIS         { end_of_function_parameter_declaration(); }
  TK_COLON
  type                         { push_function_return_type(); }
  block                        { end_function(); }
  ;

module:
  TK_MODULE
  TK_IDENTIFIER                { push_module($2); }
  statement_end
  ;

program_part:
  module
  | function
  ;

program:
  program_part
  | program program_part;

%%

void yyerror(const char *str) {
  fprintf(stderr, "Error:%s:%d:%d %s\n", inputpath, yylineno, yylloc.first_column, str);
  exit(EXIT_FAILURE);
}
