/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ----- (I.) DEFINITIONS --------------------------------------------------- */

%option noyywrap
%option yylineno

%{
  #include <stdio.h>
  #include <string.h>

  #include "../src/lib.h"

  #include "iom.tab.h"

  int yycolumn = 1;

  #define YY_USER_ACTION \
    yylloc.first_line = yylloc.last_line = yylineno; \
    yylloc.first_column = yycolumn; \
    yylloc.last_column = yycolumn + yyleng - 1; \
    yycolumn += yyleng;

%}

/* ----- (II.) RULES -------------------------------------------------------- */

%%

  /* ----- comments ----- */

"//".* {

}

  /* ----- keywords ----- */

"module" {
  printf("$TK_MODULE\n");
  return TK_MODULE;
}

"return" {
  printf("$TK_RETURN\n");
  return TK_RETURN;
}

"for"  {
  printf("$TK_FOR\n");
  return TK_FOR;
}

"in"  {
  printf("$TK_IN\n");
  return TK_IN;
}

"if"  {
  printf("$TK_IF\n");
  return TK_IF;
}

"int" {
  printf("$TK_INT\n");
  return TK_INT;
}

"real" {
  printf("$TK_REAL\n");
  return TK_REAL;
}

  /* ----- constants ----- */

[a-zA-Z_][a-zA-Z0-9_]* {
  yylval.string_val = strdup_(yytext);
  printf("$TK_IDENTIFIER %s\n", yytext);
  return TK_IDENTIFIER;
}

[1-9][0-9]*\.[0-9]* {
  yylval.real_val = atof(yytext);
  printf("$TK_CONST_REAL %f\n", yylval.real_val);
  return TK_CONST_REAL;
}

[1-9][0-9]* {
  yylval.int_val = atoi(yytext);
  printf("$TK_CONST_INT %d\n", yylval.int_val);
  return TK_CONST_INT;
}

"0" {
  yylval.int_val = 0;
  printf("$TK_CONST_INT 0\n");
  return TK_CONST_INT;
}

  /* ----- symbols ----- */

"<" {
  printf("$TK_LESS\n");
  return TK_LESS;
}

">" {
  printf("$TK_GREATER\n");
  return TK_GREATER;
}

"<=" {
  printf("$TK_LEQ\n");
  return TK_LEQ;
}

">=" {
  printf("$TK_GEQ\n");
  return TK_GEQ;
}

"==" {
  printf("$TK_EQUAL\n");
  return TK_EQUAL;
}

"+=" {
  printf("$TK_ADD_ASSIGN\n");
  return TK_ADD_ASSIGN;
}

"-=" {
  printf("$TK_SUB_ASSIGN\n");
  return TK_SUB_ASSIGN;
}

"*=" {
  printf("$TK_MUL_ASSIGN\n");
  return TK_MUL_ASSIGN;
}

"/=" {
  printf("$TK_DIV_ASSIGN\n");
  return TK_DIV_ASSIGN;
}

"=" {
  printf("$TK_ASSIGN\n");
  return TK_ASSIGN;
}

"(" {
  printf("$TK_LEFT_PARENTHESIS\n");
  return TK_LEFT_PARENTHESIS;
}

")" {
  printf("$TK_RIGHT_PARENTHESIS\n");
  return TK_RIGHT_PARENTHESIS;
}

"[" {
  printf("$TK_LEFT_BRACKET\n");
  return TK_LEFT_BRACKET;
}

"]" {
  printf("$TK_RIGHT_BRACKET\n");
  return TK_RIGHT_BRACKET;
}

"{" {
  printf("$TK_LEFT_BRACE\n");
  return TK_LEFT_BRACE;
}

"}" {
  printf("$TK_RIGHT_BRACE\n");
  return TK_RIGHT_BRACE;
}

"+" {
  printf("$TK_PLUS\n");
  return TK_PLUS;
}

"-" {
  printf("$TK_MINUS\n");
  return TK_MINUS;
}

"*" {
  printf("$TK_ASTERISK\n");
  return TK_ASTERISK;
}

"/" {
  printf("$TK_DIVIDE\n");
  return TK_DIVIDE;
}

"," {
  printf("$TK_COMMA\n");
  return TK_COMMA;
}

":" {
  printf("$TK_COLON\n");
  return TK_COLON;
}

";" {
  printf("$TK_SEMICOLON\n");
  return TK_SEMICOLON;
}

  /* ----- white spaces ----- */

" " {
  /*printf("$SPACE\n");*/
  /*return TK_SPACE;*/
}

"\t" {
  /*printf("$TAB\n");*/
  /*return TK_TABULTOR;*/
}

"\n" {
  yycolumn = 1;
  /*printf("$NEWLINE\n");*/
  /*return TK_NEWLINE;*/
}

  /* ----- unknown tokens ----- */

. {
  printf("$TK_UNEXPECTED\n");
  return TK_UNEXPECTED;
}

%%

/* ----- (III.) USER CODE ----------------------------------------------------- */
