/******************************************************************************
 *                               IOM Compiler                                 *
 *       _                               _ _                                  *
 *      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
 *      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
 *      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
 *                                 |_|                                        *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

/* ===== ICG := INTERMEDIATE CODE GENERATION ================================ */

#ifndef __ICG__H
#define __ICG__H

#include <inttypes.h>

#include <mate/common.h>

#include "2-ast.h"

/* ----- intermediate code generation --------------------------------------- */

// TODO
enum IC_Instruction_Type {
  /* corresponds to IC_Mnemonic_List in *-log.c - file */
  INSTR_ADD =
      0, // TODO: explain each instruction (refer to iom-cpu machine codes)
  INSTR_SUB = 1,
  INSTR_MUL = 2,
  INSTR_DIV = 3,
  INSTR_CMP_GREATER = 4,
  INSTR_CMP_LESS = 5,
  INSTR_CMP_GREATER_EQUAL = 6,
  INSTR_CMP_LESS_EQUAL = 7,
  INSTR_LOAD = 8,
  INSTR_STORE = 9,
  INSTR_LOAD_IMMEDIATE = 10,
  INSTR_LOAD_LOCAL = 11,
  INSTR_STORE_LOCAL = 12,
  INSTR_BRANCH = 13,
  INSTR_BRANCH_EQUAL = 14,
  INSTR_BRANCH_NOT_EQUAL = 15,
  INSTR_BRANCH_IF_ONE = 16,
  INSTR_BRANCH_IF_ZERO = 17,
  INSTR_CALL = 18,
  INSTR_RETURN = 19,
  INSTR_RETURN_WITH_VALUE = 20,
  INSTR_NOP = 21,
  INSTR_ALLOC_STACK = 22,
  INSTR_PUSH = 23,
  INSTR_POP = 24,
  INSTR_MOVE = 25
};

// TODO
char *get_mnemonic_name(enum IC_Instruction_Type opcode);

// TODO
struct IC_Instruction {
  struct Symbol *sym; /* source symbol */
  int addr;           /* address */
  int opc;            /* opcode of type IC_Instruction_Type */
  int align;          /* operand size in bytes */
  int64_t op[3];      /* operands */
  bool is_jump_dest; /* this instruction is a jump target of another instruction
                        (used for snythesis of basic blocks) */
  struct IC_Instruction *dest_instr; /* e.g. branch destination instruction */
  struct Basicblock *basicblock;     /* parent basicblock */
};

// TODO
struct IC_Instruction *push_ic_instruction(struct mate__List *list,
                                           struct Symbol *sym,
                                           enum IC_Instruction_Type opc,
                                           uint64_t op0, uint64_t op1,
                                           uint64_t op2);

// TODO
void print_ic_instruction(struct IC_Instruction *instr, int indent);

// TODO
void generate_symbol_addresses(struct Symbol_Table *sym_table,
                               int *parameter_address, int *variable_address);

// TODO
void generate_intermediate_code(struct Symbol *root);

/* -------------------------------------------------------------------------- */

#endif
