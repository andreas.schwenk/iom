/******************************************************************************
 *                            IOM Virtual Machine                             *
 *       _                                                                    *
 *      (_)___ _ __   __ ___ __                                               *
 *      | / _ \ '  \  \ V / '  \                                              *
 *      |_\___/_|_|_|  \_/|_|_|_|                                             *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#ifndef __IOM_VM_CONV__H
#define __IOM_VM_CONV__H

/* ===== CONV := FILE CONVERSION ============================================ */

#include <inttypes.h>
#include <stdbool.h>

void check_register_range(int reg);

bool encode_instr_type_A(char *line, int *addr, uint64_t *instr,
                         int *opc, uint64_t *val58);

bool encode_instr_type_B(char *line, int *addr, uint64_t *instr,
                         int *opc, int *reg1, uint64_t *val53);

bool encode_instr_type_C(char *line, int *addr, uint64_t *instr,
                         int *opc, int *reg1, int *reg2, uint64_t *val48);

bool encode_instr_type_D(char *line, int *addr, uint64_t *instr,
                         int *opc, int *reg1, int *reg2, int *reg3, uint64_t *val43);

void convert_asm_to_hex(FILE *f_in, FILE *f_out);

/* -------------------------------------------------------------------------- */

#endif
