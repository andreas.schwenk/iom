/******************************************************************************
 *                            IOM Virtual Machine                             *
 *       _                                                                    *
 *      (_)___ _ __   __ ___ __                                               *
 *      | / _ \ '  \  \ V / '  \                                              *
 *      |_\___/_|_|_|  \_/|_|_|_|                                             *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "iom-vm-defs.h"
#include "iom-vm-mne.h"

#include "iom-vm-exe.h"

extern char MNEMONICS[NUM_MNEMONICS][MAX_MNEMONIC_STR_SIZE];

void dump_registers(uint64_t *reg) {
  printf("reg: [");
  for(int i=0; i<NUM_REG; i++) {
    printf("%lld", reg[i]);
    if(i<31)
      printf(" ");
  }
  printf("]\n");
}

void execute_vm(uint64_t mem_size, uint64_t* bc, uint64_t bc_size, uint64_t bc_dst_addr, int pc_init) {
  int reg1, reg2, reg3;
  uint64_t instr, reg[NUM_REG], val;
  int64_t val_signed;
  int opc;     /* opcode */
  int pc = 0;  /* program counter */
  int addr;    /* address */

  uint8_t *mem = ALLOC(uint8_t, mem_size); /* emulation memory */

  /* init registers */
  memset(reg, 0, sizeof(uint64_t)*NUM_REG);

  /* bytecode valid? */
  if(bc == NULL) {
    fprintf(stderr, "Error: bytecode is NULL\n");
    exit(EXIT_FAILURE);
  }

  /* copy bytecode into memory */
  if(bc_dst_addr+bc_size >= mem_size) {
    fprintf(stderr, "Error: bytecode does not fit into memory\n");
    exit(EXIT_FAILURE);
  }
  memcpy(mem+bc_dst_addr, bc, sizeof(uint64_t)*bc_size);

  /* execute bytecode */
  while(true) {

    if(pc<0 || pc >= bc_size) {
      fprintf(stderr, "Error: PC (%d) is out of range\n", pc);
      exit(EXIT_FAILURE);
    }

    /* ----- decode ----- */
    instr = bc[pc];
    opc = (instr >> (64-6)) & 0x3F;
    switch(opc>>4) {
      case 0: /* type A */
        val = instr & 0x03FFFFFFFFFFFFFF;
        val_signed = ((instr>>(58-1))&0x1) ? (0xFC00000000000000 | val) : val;
        printf("$%08d: %s %llu\n", pc, MNEMONICS[opc], val);
        break;
      case 1: /* type B */
        reg1 = (instr >> (64-6-5))     & 0x1F;
        val = instr & 0x001FFFFFFFFFFFFF;
        val_signed = ((instr>>(53-1))&0x1) ? (0xFFE0000000000000 | val) : val;
/*printf("val: %llx %llu\n", val, val);
val_signed = -3;*/
//printf("val_signed: %llx %lld\n", val_signed, val_signed);
        printf("$%08d: %s r%d %llu\n", pc, MNEMONICS[opc], reg1, val);
        break;
      case 2: /* type C */
        reg1 = (instr >> (64-6-5))     & 0x1F;
        reg2 = (instr >> (64-6-5-5))   & 0x1F;
        val = instr & 0x0000FFFFFFFFFFFF;
        val_signed = ((instr>>(48-1))&0x1) ? (0xFFFF000000000000 | val) : val;
        printf("$%08d: %s r%d r%d %llu\n", pc, MNEMONICS[opc], reg1, reg2, val);
        break;
      case 3: /* type D */
        reg1 = (instr >> (64-6-5))     & 0x1F;
        reg2 = (instr >> (64-6-5-5))   & 0x1F;
        reg3 = (instr >> (64-6-5-5-5)) & 0x1F;
        val = instr & 0x000007FFFFFFFFFF;
        val_signed = ((instr>>(43-1))&0x1) ? (0xFFFFF80000000000 | val) : val;
        printf("$%08d: %s r%d r%d r%d %llu\n", pc, MNEMONICS[opc], reg1, reg2, reg3, val);
        break;
    }

    /* ----- execute ----- */
    switch(opc) {
      case OPC_SHUTDOWN:
        printf("Shutdown at PC=%d\n", pc);
        exit(EXIT_SUCCESS);
        break;
      case OPC_NO_OP:
        /* no operation */
        break;
      case OPC_JUMP:
        //printf("$%08d: jump to address %llu\n", pc, val);
        pc = val - 1;
        break;
      case OPC_CALL:
        pc = val - 1;
        //TODO: push return address to stack
        break;
      case OPC_LOAD_I:
        reg[reg1] = val_signed;
        break;
      case OPC_LOAD_IND:
        addr = reg[reg2] + val_signed;
        CHECK_ADDR();
        reg[reg1] = mem[addr];
        break;
      case OPC_ADD:
        reg[reg1] = reg[reg2] + reg[reg3];
        break;
      case OPC_SUB:
        reg[reg1] = reg[reg2] - reg[reg3];
        break;
      case OPC_MUL:
        reg[reg1] = reg[reg2] * reg[reg3];
        break;
      case OPC_DIV:
        reg[reg1] = reg[reg2] / reg[reg3];
        break;
      default:
        fprintf(stderr, "Error: unimplemented opcode %d [%s] at PC=%d\n", opc, MNEMONICS[opc], pc);
        exit(EXIT_FAILURE);
    }
    pc ++;

    dump_registers(reg);
  }

  return;
}
