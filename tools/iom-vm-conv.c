/******************************************************************************
 *                            IOM Virtual Machine                             *
 *       _                                                                    *
 *      (_)___ _ __   __ ___ __                                               *
 *      | / _ \ '  \  \ V / '  \                                              *
 *      |_\___/_|_|_|  \_/|_|_|_|                                             *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "iom-vm-mne.h"
#include "iom-vm-defs.h"

#include "iom-vm-conv.h"

int f_in_line_no;

void check_register_range(int reg) {
  if(reg<0 || reg>=NUM_REG) {
    fprintf(stderr, "Error: register has value %d and thus is out of range [0,31]\n", reg);
    exit(EXIT_FAILURE);
  }
}

/* encodes instruction type A:  opc (6 bit), val (58 bit) */
bool encode_instr_type_A(char *line, int *addr, uint64_t *instr,
                         int *opc, uint64_t *val58) {
  char mne[16];
  char val_str[16];
  sscanf(line, "$%d %s %s", addr, mne, val_str);
  if(strncmp(val_str, "0x", 2)==0)
    sscanf(val_str, "0x%llx", val58);
  else
    sscanf(val_str, "%llu", val58);
  *opc = get_mnemonic_opc(mne);
  //printf("test-1: %d\n", *opc);
  *instr =
    ((uint64_t)*opc << (64-6))   |
    (*val58 & 0x03FFFFFFFFFFFFFF);
  //printf("test-2: %llx\n", *instr);
  return true;
}

/* encodes instruction type B:  opc (6 bit), reg (5 bit), val (53 bit) */
bool encode_instr_type_B(char *line, int *addr, uint64_t *instr,
                         int *opc, int *reg1, uint64_t *val53) {
  char mne[16];
  sscanf(line, "$%d %s r%d %llu", addr, mne, reg1, val53);
  check_register_range(*reg1);
  *opc = get_mnemonic_opc(mne);
  printf("XXXXX: line %s reg %d\n", line, *reg1);
  *instr =
    ((uint64_t)*opc  << (64-6))   |
    ((uint64_t)*reg1 << (64-6-5)) |
    (*val53 & 0x001FFFFFFFFFFFFF);
  return true;
}

/* encodes instruction type C:  opc (6 bit), reg1 (5 bit), reg1 (5 bit), val (48 bit) */
bool encode_instr_type_C(char *line, int *addr, uint64_t *instr,
                         int *opc, int *reg1, int *reg2, uint64_t *val48) {
  char mne[16];
  sscanf(line, "$%d %s r%d r%d %llu", addr, mne, reg1, reg2, val48);
  check_register_range(*reg1);
  check_register_range(*reg2);
  *opc = get_mnemonic_opc(mne);
  *instr =
    ((uint64_t)*opc  << (64-6))     |
    ((uint64_t)*reg1 << (64-6-5))   |
    ((uint64_t)*reg2 << (64-6-5-5)) |
    (*val48 & 0x0000FFFFFFFFFFFF);
  return true;
}

/* encodes instruction type C:  opc (6 bit), reg1 (5 bit), reg1 (5 bit), val (48 bit) */
bool encode_instr_type_D(char *line, int *addr, uint64_t *instr,
                         int *opc, int *reg1, int *reg2, int *reg3, uint64_t *val43) {
  char mne[16];
  sscanf(line, "$%d %s r%d r%d r%d %llu", addr, mne, reg1, reg2, reg3, val43);
  check_register_range(*reg1);
  check_register_range(*reg2);
  *opc = get_mnemonic_opc(mne);
  *instr =
    ((uint64_t)*opc  << (64-6))       |
    ((uint64_t)*reg1 << (64-6-5))     |
    ((uint64_t)*reg2 << (64-6-5-5))   |
    ((uint64_t)*reg3 << (64-6-5-5-5)) |
    (*val43 & 0x000007FFFFFFFFFF);
  return true;
}

void convert_asm_to_hex(FILE *f_in, FILE *f_out) {
  uint64_t *bc = NULL; /* bytecode */
  uint64_t bc_size = 0;

  size_t linecap;
  ssize_t len;
  char *line = NULL;

  int addr;     /* address  */
  char mne[16]; /* mnemonic */
  int opc;      /* opcode   */
  uint64_t instr, val;
  int reg1, reg2, reg3; /* registers */

  f_in_line_no = 1;
  while( (len=getline(&line, &linecap, f_in)) != -1) {

    //printf("%s\n", line);

    if(len<=1 || line[0]=='#' || line[0]=='.') {
      f_in_line_no ++;
      continue;
    }

    switch(line[0]) {

      case '<': /* ----- get program size ----- */
        sscanf(line, "<%lld>\n", &bc_size);
  printf("bytecode-length: %lld\n", bc_size);
        bc = ALLOC(uint64_t, bc_size);
        memset(bc, 0, sizeof(uint64_t)*bc_size);
        break;

      case '$': /* ----- get instruction ----- */
        sscanf(line, "$%x %s", &addr, mne);
        if((addr%8) != 0) {
          fprintf(stderr, "Error: address '%08X' is not a multiple of 8\n", addr);
          exit(EXIT_FAILURE);
        }

        printf("addr: %d\n", addr);
        printf("mne: %s\n", mne);

        opc = get_mnemonic_opc(mne);
        printf("opc: %d\n", opc);

        if(       opc < 16) {
          encode_instr_type_A(line, &addr, &instr,
                              &opc, &val);
          printf("type: A\n");
        } else if(opc < 32) {
          encode_instr_type_B(line, &addr, &instr,
                              &opc, &reg1, &val);
          printf("type: B\n");
        } else if(opc < 48) {
          encode_instr_type_C(line, &addr, &instr,
                              &opc, &reg1, &reg2, &val);
          printf("type: C\n");
        } else {
          encode_instr_type_D(line, &addr, &instr,
                              &opc, &reg1, &reg2, &reg3, &val);
          printf("type: D\n");
        }

        printf("instr: %llx\n", instr);
        //printf("reconstructed OPC: %lld\n", (instr >> 58) & 0x3F);

        if(addr >= bc_size) {
          fprintf(stderr, "Error: address '%d' is too large (max: '%llu')\n", addr, bc_size);
          exit(EXIT_FAILURE);
        }

        bc[addr] = instr;

        break;

      default:
        fprintf(stderr, "Error: failed to process line %d\n", f_in_line_no);
        exit(EXIT_SUCCESS);
    }

    f_in_line_no ++;
    printf("-------------------\n");
  }

  fwrite(bc, sizeof(uint64_t)*bc_size, 1, f_out);

  if(bc != NULL)
    free(bc);
}
