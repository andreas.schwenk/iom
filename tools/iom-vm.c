/******************************************************************************
 *                            IOM Virtual Machine                             *
 *       _                                                                    *
 *      (_)___ _ __   __ ___ __                                               *
 *      | / _ \ '  \  \ V / '  \                                              *
 *      |_\___/_|_|_|  \_/|_|_|_|                                             *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include "iom-vm-defs.h"
#include "iom-vm-conv.h"
#include "iom-vm-exe.h"

void print_usage(void) {
  fprintf(stderr, "Usage: iom-vm [-h] file [file_out]\n");
}

int main(int argc, char *argv[]) {
  printf("IOM-VM -- Copyright 2018 Andreas Schwenk - GPL 3 LICENSE\n");

  int opt;
  char mode = ' ';

  char *inputpath = NULL;
  char *outputpath = NULL;

  uint64_t *bc = NULL; /* bytecode */
  uint64_t bc_size = 0;

  while((opt=getopt(argc, argv, "h")) != -1) {
    switch(opt) {
      case 'h':
        mode = 'h';
        break;
      default:
        print_usage();
        exit(EXIT_FAILURE);
    }
  }

  if( (mode=='h' && argc-optind != 2) || (mode==' ' && argc-optind != 1) ) {
    print_usage();
    exit(EXIT_FAILURE);
  }

  inputpath = argv[optind];
  FILE *f_in = fopen(inputpath, mode=='h'?"r":"rb");
  if(f_in == NULL) {
    fprintf(stderr, "Error: invalid input path '%s'\n", inputpath);
    exit(EXIT_FAILURE);
  }

  switch(mode) {
    case ' ': /* execute bytecode from input file */
      fseek(f_in, 0L, SEEK_END);
      bc_size = ftell(f_in);
      fseek(f_in, 0L, SEEK_SET);
      bc = ALLOC(uint64_t, bc_size);
      fread(bc, bc_size, 1, f_in);
      execute_vm(/*TODO:memsize*/1024, bc, bc_size, /*TODO:bc-dest-addr*/0, /*TODO:bc-init*/0);
      break;
    case 'h': /* convert content from assembler input file to hex output file */
      outputpath = argv[optind+1];
      FILE *f_out = fopen(outputpath, "wb");
      if(f_in == NULL) {
        fprintf(stderr, "Error: cannot write to output path '%s'\n", outputpath);
        exit(EXIT_FAILURE);
      }
      convert_asm_to_hex(f_in, f_out);
      fclose(f_out);
      printf("Successfully converted asm file '%s' to hex file '%s'\n", inputpath, outputpath);
      break;
  }

  fclose(f_in);

  if(bc != NULL)
    free(bc);

  exit(EXIT_SUCCESS);
}
