/******************************************************************************
 *                            IOM Virtual Machine                             *
 *       _                                                                    *
 *      (_)___ _ __   __ ___ __                                               *
 *      | / _ \ '  \  \ V / '  \                                              *
 *      |_\___/_|_|_|  \_/|_|_|_|                                             *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "iom-vm-defs.h"

#include "iom-vm-mne.h"

char MNEMONICS[NUM_MNEMONICS][MAX_MNEMONIC_STR_SIZE] = {
  /*  0-15: instuction type A */
  "shutdown", "no-op", "jump", "call", "", "", "", "", "", "", "", "", "", "", "", "",
  /* 16-31: instuction type B */
  "load", "store", "load_upper_i", "load_i", "", "", "", "", "", "", "", "", "", "", "", "",
  /* 32-47: instuction type C */
  "move", "load_ind", "store_ind", "", "", "", "", "", "", "", "", "", "", "", "", "",
  /* 48-63: instuction type D */
  "add", "sub", "mul", "div", "f_add", "f_sub", "f_mul", "f_div", "", "", "", "", "", "", "", ""
};

int get_mnemonic_opc(char *mne) {
  for(int i=0; i<NUM_MNEMONICS; i++) {
    if(strcmp(MNEMONICS[i], mne)==0)
      return i;
  }
  fprintf(stderr, "error: unknown mnemonic '%s'\n", mne);
  exit(0);
}
