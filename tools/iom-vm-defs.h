/******************************************************************************
 *                            IOM Virtual Machine                             *
 *       _                                                                    *
 *      (_)___ _ __   __ ___ __                                               *
 *      | / _ \ '  \  \ V / '  \                                              *
 *      |_\___/_|_|_|  \_/|_|_|_|                                             *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#ifndef __IOM_VM_DEFS__H
#define __IOM_VM_DEFS__H

/* ===== DEFS := DEFINITIONS ================================================ */

#include <stdlib.h>

#define ALLOC(TYPE,CNT) (TYPE*)malloc(sizeof(TYPE)*CNT)

/* ----- machine definition ------------------------------------------------- */

#define MAX_MNEMONIC_STR_SIZE 16
#define NUM_MNEMONICS 64
#define NUM_REG 32

/* ------instruction definition --------------------------------------------- */

/*  0-15: instruction type A */
#define OPC_SHUTDOWN      0
#define OPC_NO_OP         1
#define OPC_JUMP          2
#define OPC_CALL          3

/* 16-31: instruction type B */
#define OPC_LOAD         16
#define OPC_STORE        17
#define OPC_LOAD_UPPER_I 18
#define OPC_LOAD_I       19

/* 32-47: instruction type C */
#define OPC_MOVE         32
#define OPC_LOAD_IND     33
#define OPC_STORE_IND    34

/* 48-63: instruction type D */
#define OPC_ADD          48
#define OPC_SUB          49
#define OPC_MUL          50
#define OPC_DIV          51
#define OPC_FADD         52
#define OPC_FSUB         53
#define OPC_FMUL         54
#define OPC_FDIV         55

/* -------------------------------------------------------------------------- */

#endif
