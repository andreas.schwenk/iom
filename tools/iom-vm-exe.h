/******************************************************************************
 *                            IOM Virtual Machine                             *
 *       _                                                                    *
 *      (_)___ _ __   __ ___ __                                               *
 *      | / _ \ '  \  \ V / '  \                                              *
 *      |_\___/_|_|_|  \_/|_|_|_|                                             *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/

#ifndef __IOM_VM_EXE__H
#define __IOM_VM_EXE__H

/* ===== EXE := EXECUTABLE / SIMULATOR ====================================== */

#include <inttypes.h>

#define CHECK_ADDR(void) \
  if(addr < 0 || addr >= mem_size) { \
    fprintf(stderr, "Error: address %d at PC=%d is invalid\n", addr, pc); \
    exit(EXIT_FAILURE); \
  }

void dump_registers(uint64_t *reg);

void execute_vm(uint64_t mem_size, uint64_t* bc, uint64_t bc_size, uint64_t bc_dst_addr, int pc_init);

#endif
