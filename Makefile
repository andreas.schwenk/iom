#******************************************************************************
#*                               IOM Compiler                                 *
#*       _                               _ _                                  *
#*      (_)___ _ __    __ ___ _ __  _ __(_) |___ _ _                          *
#*      | / _ \ '  \  / _/ _ \ '  \| '_ \ | / -_) '_|                         *
#*      |_\___/_|_|_| \__\___/_|_|_| .__/_|_\___|_|                           *
#*                                 |_|                                        *
#*                                                                            *
#* Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
#*                                                                            *
#* GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
#*                                                                            *
#* This library is licensed as described in LICENSE, which you should have    *
#* received as part of this distribution.                                     *
#*                                                                            *
#* This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
#* KIND, either impressed or implied.                                         *
#******************************************************************************/

# ----- execuable name ---------------------------------------------------------

PROG = iom-c

# ----- configuration ----------------------------------------------------------

CC = gcc
FLEX = flex
BISON = /usr/bin/bison       # /usr/local/opt/bison/bin/bison

# ----- directories ------------------------------------------------------------

DIR_SRC = src/
DIR_TST = tests/
BUILD_DIR = build/

# ----- dependencies -----------------------------------------------------------

INC = -I/usr/local/lib/
LIB = -lm -L/usr/local/lib/ -lmate

HEADERS_1  = $(DIR_SRC)config.h
HEADERS_1  = $(DIR_SRC)lib.h
HEADERS_1 += $(DIR_SRC)2-ast.h
HEADERS_1 += $(DIR_SRC)3-sa.h
HEADERS_1 += $(DIR_SRC)4-icg.h
HEADERS_1 += $(DIR_SRC)5-opt.h
HEADERS_1 += $(DIR_SRC)6-mcg.h
HEADERS_1 += $(DIR_SRC)6-mcg-iomcpu.h

HEADERS_2 = $(BUILD_DIR)iom.tab.h

HEADERS = $(HEADERS_1) $(HEADERS_2)
HEADERS_WO_PTABH = $(HEADERS_1)

OBJS =  $(BUILD_DIR)lex.yy.o $(BUILD_DIR)iom.tab.o
OBJS += $(BUILD_DIR)0-main.o
OBJS += $(BUILD_DIR)2-ast.o $(BUILD_DIR)2-ast-expression.o $(BUILD_DIR)2-ast-function.o $(BUILD_DIR)2-ast-if.o $(BUILD_DIR)2-ast-log.o $(BUILD_DIR)2-ast-loop.o $(BUILD_DIR)2-ast-symbol.o $(BUILD_DIR)2-ast-symboltable.o
OBJS += $(BUILD_DIR)3-sa.o
OBJS += $(BUILD_DIR)4-icg.o $(BUILD_DIR)4-icg-log.o
OBJS += $(BUILD_DIR)5-opt.o
OBJS += $(BUILD_DIR)6-mcg.o $(BUILD_DIR)6-mcg-iomcpu.o $(BUILD_DIR)6-mcg-iomcpu-log.o
OBJS += $(BUILD_DIR)lib.o

# ----- flags ------------------------------------------------------------------

#CFLAGS = -O3           # optimize level 3
CFLAGS = -std=gnu99 -g3   # debug level 3         TODO: switch debug/release via configure

# ----- make build dir ---------------------------------------------------------

$(shell mkdir -p $(BUILD_DIR))

# ----- build execuable --------------------------------------------------------

$(PROG): $(OBJS)
	$(CC) $(OBJS) $(INC) $(LIB) -o $(BUILD_DIR)$(PROG)

# ----- flex and bison ---------------------------------------------------------

$(BUILD_DIR)iom.tab.c $(BUILD_DIR)iom.tab.h: $(DIR_SRC)2-iom.y $(HEADERS_WO_PTABH)
	$(BISON) -v -d -o $(BUILD_DIR)iom.tab.c $(DIR_SRC)2-iom.y

$(BUILD_DIR)lex.yy.c: $(DIR_SRC)1-iom.l $(HEADERS)
	$(FLEX) -o $(BUILD_DIR)lex.yy.c $(DIR_SRC)1-iom.l

# ----- build object files -----------------------------------------------------

$(BUILD_DIR)0-main.o: $(DIR_SRC)0-main.c $(HEADERS)
	$(CC) $(DIR_SRC)0-main.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)0-main.o

$(BUILD_DIR)lex.yy.o: $(BUILD_DIR)lex.yy.c $(HEADERS)
	$(CC) $(BUILD_DIR)lex.yy.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)lex.yy.o

$(BUILD_DIR)iom.tab.o: $(BUILD_DIR)iom.tab.c $(HEADERS)
	$(CC) $(BUILD_DIR)iom.tab.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)iom.tab.o

$(BUILD_DIR)2-ast-expression.o: $(DIR_SRC)2-ast-expression.c $(HEADERS)
	$(CC) $(DIR_SRC)2-ast-expression.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)2-ast-expression.o
$(BUILD_DIR)2-ast-function.o: $(DIR_SRC)2-ast-function.c $(HEADERS)
	$(CC) $(DIR_SRC)2-ast-function.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)2-ast-function.o
$(BUILD_DIR)2-ast-if.o: $(DIR_SRC)2-ast-if.c $(HEADERS)
	$(CC) $(DIR_SRC)2-ast-if.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)2-ast-if.o
$(BUILD_DIR)2-ast-log.o: $(DIR_SRC)2-ast-log.c $(HEADERS)
	$(CC) $(DIR_SRC)2-ast-log.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)2-ast-log.o
$(BUILD_DIR)2-ast-loop.o: $(DIR_SRC)2-ast-loop.c $(HEADERS)
	$(CC) $(DIR_SRC)2-ast-loop.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)2-ast-loop.o
$(BUILD_DIR)2-ast-symbol.o: $(DIR_SRC)2-ast-symbol.c $(HEADERS)
	$(CC) $(DIR_SRC)2-ast-symbol.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)2-ast-symbol.o
$(BUILD_DIR)2-ast-symboltable.o: $(DIR_SRC)2-ast-symboltable.c $(HEADERS)
	$(CC) $(DIR_SRC)2-ast-symboltable.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)2-ast-symboltable.o
$(BUILD_DIR)2-ast.o: $(DIR_SRC)2-ast.c $(HEADERS)
	$(CC) $(DIR_SRC)2-ast.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)2-ast.o

$(BUILD_DIR)3-sa.o: $(DIR_SRC)3-sa.c $(HEADERS)
	$(CC) $(DIR_SRC)3-sa.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)3-sa.o

$(BUILD_DIR)4-icg.o: $(DIR_SRC)4-icg.c $(HEADERS)
	$(CC) $(DIR_SRC)4-icg.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)4-icg.o
$(BUILD_DIR)4-icg-log.o: $(DIR_SRC)4-icg-log.c $(HEADERS)
	$(CC) $(DIR_SRC)4-icg-log.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)4-icg-log.o

$(BUILD_DIR)5-opt.o: $(DIR_SRC)5-opt.c $(HEADERS)
	$(CC) $(DIR_SRC)5-opt.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)5-opt.o

$(BUILD_DIR)6-mcg.o: $(DIR_SRC)6-mcg.c $(HEADERS)
	$(CC) $(DIR_SRC)6-mcg.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)6-mcg.o
$(BUILD_DIR)6-mcg-iomcpu.o: $(DIR_SRC)6-mcg-iomcpu.c $(HEADERS)
	$(CC) $(DIR_SRC)6-mcg-iomcpu.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)6-mcg-iomcpu.o
$(BUILD_DIR)6-mcg-iomcpu-log.o: $(DIR_SRC)6-mcg-iomcpu-log.c $(HEADERS)
	$(CC) $(DIR_SRC)6-mcg-iomcpu-log.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)6-mcg-iomcpu-log.o

$(BUILD_DIR)lib.o: $(DIR_SRC)lib.c $(HEADERS)
	$(CC) $(DIR_SRC)lib.c $(INC) $(CFLAGS) -c -o $(BUILD_DIR)lib.o

# ----- clean ------------------------------------------------------------------

clean:
	rm -rf $(BUILD_DIR)

# ----- installation -----------------------------------------------------------

# TODO (migrate from "mate")
# .PHONY: install
# install: $(PROG)
# 		#cp $< /usr/local/bin/$(PROG)
# 		#cp iom-c.1 /usr/local/share/man/man1/   # TODO: create man pages!
# 		# TODO: copy  libarary and headers

# ------------------------------------------------------------------------------
