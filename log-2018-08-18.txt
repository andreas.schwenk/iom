IOM-COMPILER -- Copyright 2018 Andreas Schwenk - GPL 3 LICENSE
---------- TOKEN LIST ----------
$TK_MODULE
$TK_IDENTIFIER test
$TK_SEMICOLON
$TK_PLUS
$TK_IDENTIFIER faculty
$TK_LEFT_PARENTHESIS
$TK_IDENTIFIER x
$TK_COLON
$TK_INT
$TK_RIGHT_PARENTHESIS
$TK_COLON
$TK_INT
$TK_LEFT_BRACE
$TK_IDENTIFIER res
$TK_ASSIGN
$TK_CONST_INT 1
$TK_SEMICOLON
$TK_FOR
$TK_IDENTIFIER i
$TK_IN
$TK_LEFT_BRACKET
$TK_CONST_INT 1
$TK_COMMA
$TK_IDENTIFIER x
$TK_RIGHT_BRACKET
$TK_IDENTIFIER res
$TK_ASSIGN
$TK_IDENTIFIER res
$TK_ASTERISK
$TK_IDENTIFIER i
$TK_SEMICOLON
$TK_RETURN
$TK_IDENTIFIER res
$TK_SEMICOLON
$TK_RIGHT_BRACE
$TK_PLUS
$TK_IDENTIFIER main
$TK_LEFT_PARENTHESIS
$TK_RIGHT_PARENTHESIS
$TK_COLON
$TK_INT
$TK_LEFT_BRACE
$TK_IDENTIFIER f
$TK_ASSIGN
$TK_IDENTIFIER faculty
$TK_LEFT_PARENTHESIS
$TK_CONST_INT 5
$TK_COMMA
$TK_CONST_INT 7
$TK_PLUS
$TK_CONST_INT 3
$TK_ASTERISK
$TK_CONST_INT 2
$TK_COMMA
$TK_CONST_INT 8
$TK_COMMA
$TK_CONST_INT 9
$TK_RIGHT_PARENTHESIS
$TK_SEMICOLON
$TK_IF
$TK_LEFT_PARENTHESIS
$TK_IDENTIFIER f
$TK_GREATER
$TK_CONST_INT 7
$TK_RIGHT_PARENTHESIS
$TK_IDENTIFIER f
$TK_ASSIGN
$TK_IDENTIFIER f
$TK_PLUS
$TK_CONST_INT 1
$TK_SEMICOLON
$TK_RETURN
$TK_CONST_INT 0
$TK_SEMICOLON
$TK_RIGHT_BRACE
---------- ABSTRACT SYNTAX TREE ----------
SYM_ROOT: id='root'
--------------------------------
| SYMBOL-TABLE 0: (parent=-1 children={1 3 })
|  faculty : SYM_FUNCTION, TYPE_INT, location=0
|  main : SYM_FUNCTION, TYPE_INT, location=0
--------------------------------
    SYM_MODULE: id='test'
    SYM_FUNCTION: id='faculty' : TYPE_INT
    --------------------------------
    | SYMBOL-TABLE 1: (parent=0 children={2 })
    |  x : SYM_FUNCTION_PARAM, TYPE_INT, location=0
    |  res : SYM_LOCAL_VARIABLE, TYPE_INT, location=-8
    --------------------------------
    $00000000 ALLOC_STACK 16                   # alloc(16)                         playground.iom:4
    $00000001 LOAD_IMMEDIATE r0 1              # r0 := 1                           playground.iom:5
    $00000002 STORE_LOCAL r0 FP-8              # res := r0                         playground.iom:5
    $00000003 LOAD_IMMEDIATE r0 1              # r0 := 1                           playground.iom:6
    $00000004 STORE_LOCAL r0 FP-16             # i := r0                           playground.iom:6
    $00000005 LOAD_LOCAL r0 FP-16              # r0 := i                           playground.iom:6
    $00000006 LOAD_LOCAL r1 FP+0               # r1 := x                           playground.iom:4
    $00000007 CMP_GREATER_EQUAL r1 r0 r1       # r1 := r0 >= r1                    playground.iom:6
    $00000008 BRANCH_IF_ZERO r0 0 $00000018    # if(r0==0) { jump $00000018 }      playground.iom:6
    $00000009 LOAD_LOCAL r0 FP-8               # r0 := res                         playground.iom:5
    $00000010 LOAD_LOCAL r1 FP-16              # r1 := i                           playground.iom:6
    $00000011 MUL r0 r0 r1                     # r0 := r0 * r1                     playground.iom:7
    $00000012 STORE_LOCAL r0 FP-8              # res := r0                         playground.iom:5
    $00000013 LOAD_LOCAL r0 FP-16              # r0 := i                           playground.iom:6
    $00000014 LOAD_IMMEDIATE r1 0              # r1 := 0                           playground.iom:6
    $00000015 ADD r0 r0 r1                     # r0 := r0 + r1                     playground.iom:6
    $00000016 STORE_LOCAL r0 FP-16             # i := r0                           playground.iom:6
    $00000017 BRANCH $00000005                 # jump $00000005                    playground.iom:6
    $00000018 LOAD_LOCAL r0 FP-8               # r0 := res                         playground.iom:5
    $00000019 RETURN_WITH_VALUE                # return r0                         playground.iom:8
    --------------------------------
        SYM_FUNCTION_PARAM: id='x' : TYPE_INT
        SYM_FUNCTION_BEGIN:
        SYM_ASSIGNMENT: id='='
            SYM_LOCAL_VARIABLE: id='res' : TYPE_INT (@sym-table 1)
            SYM_CONST_INTEGER: id='1' value='1' : TYPE_INT
        SYM_FOR_LOOP: id='for'
        --------------------------------
        | SYMBOL-TABLE 2: (parent=1 children={})
        |  i : SYM_LOCAL_VARIABLE, TYPE_INT, location=-16
        --------------------------------
            SYM_LOCAL_VARIABLE: id='i' : TYPE_INT (@sym-table 2)
            SYM_CONST_INTEGER: id='1' value='1' : TYPE_INT
            SYM_FUNCTION_PARAM: id='x' : TYPE_INT
            SYM_ASSIGNMENT: id='='
                SYM_LOCAL_VARIABLE: id='res' : TYPE_INT (@sym-table 1)
                SYM_MULTIPLY: id='*'
                    SYM_LOCAL_VARIABLE: id='res' : TYPE_INT (@sym-table 1)
                    SYM_LOCAL_VARIABLE: id='i' : TYPE_INT (@sym-table 2)
        SYM_RETURN: id='return'
            SYM_LOCAL_VARIABLE: id='res' : TYPE_INT (@sym-table 1)
    SYM_FUNCTION: id='main' : TYPE_INT
    --------------------------------
    | SYMBOL-TABLE 3: (parent=0 children={4 })
    |  f : SYM_LOCAL_VARIABLE, TYPE_INT, location=-8
    --------------------------------
    $00000020 ALLOC_STACK 8                    # alloc(8)                          playground.iom:11
    $00000021 LOAD_IMMEDIATE r0 5              # r0 := 5                           playground.iom:12
    $00000022 PUSH r0                          # push(r0)                          playground.iom:12
    $00000023 LOAD_IMMEDIATE r0 7              # r0 := 7                           playground.iom:12
    $00000024 LOAD_IMMEDIATE r1 3              # r1 := 3                           playground.iom:12
    $00000025 LOAD_IMMEDIATE r2 2              # r2 := 2                           playground.iom:12
    $00000026 MUL r1 r1 r2                     # r1 := r1 * r2                     playground.iom:12
    $00000027 ADD r0 r0 r1                     # r0 := r0 + r1                     playground.iom:12
    $00000028 PUSH r0                          # push(r0)                          playground.iom:12
    $00000029 LOAD_IMMEDIATE r0 8              # r0 := 8                           playground.iom:12
    $00000030 PUSH r0                          # push(r0)                          playground.iom:12
    $00000031 LOAD_IMMEDIATE r0 9              # r0 := 9                           playground.iom:12
    $00000032 PUSH r0                          # push(r0)                          playground.iom:12
    $00000033 CALL $00000000                   # call faculty                      playground.iom:12
    $00000034 POP 4                            # pop(4)                            playground.iom:12
    $00000035 STORE_LOCAL r0 FP-8              # f := r0                           playground.iom:12
    $00000036 LOAD_LOCAL r0 FP-8               # r0 := f                           playground.iom:12
    $00000037 LOAD_IMMEDIATE r1 7              # r1 := 7                           playground.iom:13
    $00000038 CMP_GREATER r0 r0 r1             # r0 := r0 > r1                     playground.iom:13
    $00000039 BRANCH_IF_ZERO r0 0 $00000044    # if(r0==0) { jump $00000044 }      playground.iom:13
    $00000040 LOAD_LOCAL r0 FP-8               # r0 := f                           playground.iom:12
    $00000041 LOAD_IMMEDIATE r1 1              # r1 := 1                           playground.iom:14
    $00000042 ADD r0 r0 r1                     # r0 := r0 + r1                     playground.iom:14
    $00000043 STORE_LOCAL r0 FP-8              # f := r0                           playground.iom:12
    $00000044 LOAD_IMMEDIATE r0 0              # r0 := 0                           playground.iom:15
    $00000045 RETURN_WITH_VALUE                # return r0                         playground.iom:15
    --------------------------------
        SYM_FUNCTION_BEGIN:
        SYM_ASSIGNMENT: id='='
            SYM_LOCAL_VARIABLE: id='f' : TYPE_INT (@sym-table 3)
            SYM_FUNCTION_CALL: id='faculty'
                SYM_CONST_INTEGER: id='5' value='5' : TYPE_INT
                SYM_ADD: id='+'
                    SYM_CONST_INTEGER: id='7' value='7' : TYPE_INT
                    SYM_MULTIPLY: id='*'
                        SYM_CONST_INTEGER: id='3' value='3' : TYPE_INT
                        SYM_CONST_INTEGER: id='2' value='2' : TYPE_INT
                SYM_CONST_INTEGER: id='8' value='8' : TYPE_INT
                SYM_CONST_INTEGER: id='9' value='9' : TYPE_INT
        SYM_IF_STATEMENT: id='if'
        --------------------------------
        | SYMBOL-TABLE 4: (parent=3 children={})
        --------------------------------
            SYM_GREATER: id='>'
                SYM_LOCAL_VARIABLE: id='f' : TYPE_INT (@sym-table 3)
                SYM_CONST_INTEGER: id='7' value='7' : TYPE_INT
            SYM_ASSIGNMENT: id='='
                SYM_LOCAL_VARIABLE: id='f' : TYPE_INT (@sym-table 3)
                SYM_ADD: id='+'
                    SYM_LOCAL_VARIABLE: id='f' : TYPE_INT (@sym-table 3)
                    SYM_CONST_INTEGER: id='1' value='1' : TYPE_INT
        SYM_RETURN: id='return'
            SYM_CONST_INTEGER: id='0' value='0' : TYPE_INT
