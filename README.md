```
/******************************************************************************
 *                                    IOM                                     *
 *       _                                                                    *
 *      (_)___ _ __                                                           *
 *      | / _ \ '  \                                                          *
 *      |_\___/_|_|_|                                                         *
 *                                                                            *
 * Copyright (c) 2018, Andreas Schwenk, contact@compiler-construction.com     *
 *                                                                            *
 * GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007                         *
 *                                                                            *
 * This library is licensed as described in LICENSE, which you should have    *
 * received as part of this distribution.                                     *
 *                                                                            *
 * This software is distributed on "AS IS" basis, WITHOUT WARRENTY OF ANY     *
 * KIND, either impressed or implied.                                         *
 ******************************************************************************/
```

IOM (Esperanto "a little") is a small exemplary General-Purpose Programming
Language (GPL) that I use in my lecture "Compiler Construction and Domain-
Specific Languages" (master's course) at TH Köln - University of Applied
Sciences (www.th-koeln.de).


W O R K   I N   P R O G R E S S:
Especially machine code generation and the documentation will be extended soon.


Software Architecture
=====================

The iom compiler toolchain consists of the compiler *iom-c* and the virtual
machine *iom-vm* which are described in the following sections

IOM-C
-----

### (I.) Data Structures and Control

The following files define essential data structures and manage the processing
pipeline described in (II.).

###### main.c

Pipeline Control

###### st.c, st.h

Symbol Table (ST)

###### ast.c, ast.h

Abstract Syntax Tree (AST)


### (II.) Pipeline

The following files each implement one phase of the compilation. We group phases
that are executed interleaved. The following groups each represent *one* phase:
{1,2,3,4}, {5}, {6}.

###### PHASE 1

**Files: 1-iom.l**

Lexer; expressed in *Flex* (lex) format

###### PHASE 2:

**Files: 2-iom.y 2-ast.c 2-ast.h**

Parser; expressed in *GNU Bison* (yacc) format

###### PHASE 3:

**Files: 3-sa.c, 3-sa.h**

Semantic Analysis (SA).

###### PHASE 4:

**Files: 4-icg.c, 4-icg.h**

Intermediate Code Generation (ICG). Performed after each function or block.

###### PHASE 5:

**Files: 5-opt.c, 5-opt.c**

Intermediate Code Optimization (OPT)

###### PHASE 6:

**Files: 6-mcg.c, 6-mcg.h**

Machine Code Generation (MCG)

IOM-VM
------

TODO
